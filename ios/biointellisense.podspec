#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint biointellisense.podspec` to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'biointellisense'
  s.version          = '0.0.5'
  s.summary          = 'A new flutter plugin project.'
  s.description      = <<-DESC
A new flutter plugin project.
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'email@example.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.dependency 'Flutter'
  s.platform = :ios, '8.0'

  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
  s.swift_version = '5.0'

  s.dependency 'CocoaLumberjack/Swift', '3.7.0'
  s.dependency 'Alamofire', '~> 5.4.1'
  s.dependency 'TrueTime', '~> 5.0.3'
  s.dependency 'SwiftProtobuf', '~> 1.14.0'
  s.dependency 'Zip', '~> 2.1.1'
  s.dependency 'LaunchDarkly'
  
  s.ios.vendored_frameworks = 'BioIntelliSenseSDK.framework'
  s.vendored_frameworks = 'BioIntelliSenseSDK.framework'  
  
  #s.dependency 'BioIntelliSenseSDK' './BioIntelliSenseSDK.podspec'
#  s.pod_target_xcconfig = { 'OTHER_LDFLAGS' => '-framework BioIntelliSenseSDK.framework' }
#
#  s.preserve_paths = 'BioIntelliSenseSDK.framework'
#  s.vendored_frameworks = 'BioIntelliSenseSDK.framework'

end
