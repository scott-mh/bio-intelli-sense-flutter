zipfile = "#{__dir__}/BioIntelliSenseSDK.zip"

Pod::Spec.new do |s|
  s.name                = 'BioIntelliSenseSDK'
  s.version             = '1.3.0'
  s.license             =  { :type => 'BSD' }
  s.homepage            = 'http://www.biointellisense.com'
  s.authors             = { 'developer' => 'support@biointellisense.com' }
  s.summary             = 'A cocoa pod containing the BioIntelliSense framework.'
  s.source              = { :http => 'file:' + __dir__ + '/BioIntelliSenseSDK.zip' }
  s.source_files        = 'Classes/*.{h,m}'

  s.vendored_frameworks = 'BioIntelliSenseSDK.framework'
end