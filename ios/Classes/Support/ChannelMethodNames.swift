//
//  ChannelNames.swift
//  biointellisense
//
//  Created by Scott Cornell on 9/13/21.
//

import Foundation

struct ChannelMethodNames{
    static let methodChannelName = "BioIntelliSense"
    static let startService = "startService"
    static let stopService = "stopService"
    static let startScanManual = "startScanManual"
    static let stopScan = "stopScan"
    static let retrieveQueuedDevices = "retrieveQueuedDevices"
    static let retrievePreviouslyConnectedDevices = "retrievePreviouslyConnectedDevices"
    static let retrieveDeviceHistory = "retrieveDeviceHistory"
    static let retrieveConnectionState = "retrieveConnectionState"
    static let retrievePairedDevice = "retrievePairedDevice"
    static let setScanMode = "setScanMode"
    static let verifyBioId = "verifyBioId"
    static let startScanForDevice = "startScanForDevice"
    static let retrieveBioHubId = "retrieveBioHubId"
    static let retrieveLastSyncTime = "retrieveLastSyncTime"
    static let retrieveFirmwarePlatformVersion = "retrieveFirmwarePlatformVersion"
    static let retrieveTargetPlatformVersion = "retrieveTargetPlatformVersion"
    static let retrieveTargetSDKVersion = "retrieveTargetSDKVersion"
    static let retrieveDeviceConfigurationName = "retrieveDeviceConfigurationName"
    static let generateCSVHistory = "generateCSVHistory"
    static let isScanning = "isScanning"
    static let enableNotification = "enableNotification"
    static let isNotificationChannelDisabled = "isNotificationChannelDisabled"
    static let reset = "reset"
    static let syncTimeWithServer = "syncTimeWithServer"
}
