
struct ChannelEventNames{
    
    static let serviceStatus = "BioIntelliSense/ServiceStatus"
    static let connectionState = "BioIntelliSense/ConnectionState"
    static let scanState = "BioIntelliSense/ScanState"
    static let device = "BioIntelliSense/device"
    static let error = "BioIntelliSense/error"
    static let verifyBioId = "BioIntelliSense/verifyBioId"
    static let activation = "BioIntelliSense/activation"
    static let firmware = "BioIntelliSense/firmware"
    static let bioHubId = "BioIntelliSense/bioHubId"
    static let lastSyncTime = "BioIntelliSense/lastSyncTime"
    static let syncHistoryCSV = "BioIntelliSense/syncHistoryCSV"
    static let firmwareAvailabilityChannelName = "BioIntelliSense/firmwareAvailability"
}
