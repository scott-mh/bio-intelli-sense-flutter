#import "BiointellisensePlugin.h"
#if __has_include(<biointellisense/biointellisense-Swift.h>)
#import <biointellisense/biointellisense-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "biointellisense-Swift.h"
#endif

@implementation BiointellisensePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftBiointellisensePlugin registerWithRegistrar:registrar];
}
@end
