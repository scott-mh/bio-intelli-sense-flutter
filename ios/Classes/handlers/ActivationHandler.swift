import Foundation
import Flutter
import BioIntelliSenseSDK

class ActivationHandler: BaseHandler {
        
    func onFailed(_ bioDevice: BioDevice){
        var map = [String: Any?]()
        map["status"] = "FAILED"
        map["bioId"] =  bioDevice.bioID
    
        self.eventSink?(map)
    }
    
    func onEvent(_ activationResult: DeviceActivationResult, bioDevice: BioDevice){
       
        var map = [String: Any?]()
        map["status"] = activationResult.rawValue
        map["bioId"] = bioDevice.bioID
    
        self.eventSink?(map)
    }

}
