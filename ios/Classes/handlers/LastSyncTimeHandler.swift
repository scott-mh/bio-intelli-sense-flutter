import Foundation
import Flutter
import BioIntelliSenseSDK

class LastSyncTimeHandler:BaseHandler  {
   
    func onEvent(_ time: Double){
        self.eventSink?(time)
    }

}
