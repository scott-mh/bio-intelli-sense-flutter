import Foundation
import Flutter
import BioIntelliSenseSDK

class FirmwareHandler:BaseHandler  {
    
    func onEvent(_ name: String?, progress: Double?){
        onEvent(name: name, progress: progress, updated: nil, errorCode: nil, errorMessage: nil)
    }
    
    func onEvent(_ updated: Bool?){
        onEvent(name: nil, progress: nil, updated: updated, errorCode: nil, errorMessage: nil)
    }
    
    func onEvent(_ errorCode: Int?, errorMessage: String?){
        onEvent(name: nil, progress: nil, updated: nil, errorCode: errorCode, errorMessage: errorMessage)
    }
    
    private func onEvent(name: String?, progress: Double?, updated: Bool?, errorCode: Int?, errorMessage: String?){
        
        var map = [String: Any?]()
        map["name"] = name
        map["progress"] = progress
        map["updated"] = updated
        map["errorCode"] = errorCode
        map["errorMessage"] = errorMessage
        
        self.eventSink?(map)
        
    }

}
