import Foundation
import Flutter
import BioIntelliSenseSDK

class ScanStateHandler:BaseHandler  {
       
    func onEvent( _ isScanning: Bool){
        self.eventSink?(isScanning)
    }

}
