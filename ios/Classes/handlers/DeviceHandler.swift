import Foundation
import Flutter
import BioIntelliSenseSDK

struct DeviceEvent {
    static let DISCOVERED = "DISCOVERED"
    static let STATE_CHANGE = "STATE_CHANGE"
    static let SYNC_PROGRESS_CHANGE = "SYNC_PROGRESS_CHANGE"
    static let QUEUED_DEVICE_RECEIVED = "QUEUED_DEVICE_RECEIVED"
    static let PREVIOUSLY_CONNECTED_DEVICES_RECEIVED = "PREVIOUSLY_CONNECTED_DEVICES_RECEIVED"
    static let SYNC_HISTORY_RECEIVED = "SYNC_HISTORY_RECEIVED"
}

class DeviceHandler:BaseHandler  {
   
    func onEvent(_ event: String, device: BioDevice){
        var map = [String: Any?]()
        map["event"] = event
        map["devices"] = [device.generateMap()]
        self.eventSink?(map)
    }
    
}

extension BioDevice {
    func generateMap()-> [String: Any?]{
        var map = [String: Any?]()
        map["bioId"] = self.bioID
        map["state"] = self.state.description
        map["lastSyncTime"] = self.lastSyncTime
        map["syncStartTime"] = self.syncDuration
        map["syncProgress"] = self.syncProgress
        map["uploadedBytes"] = self.uploadedBytes
        map["syncDuration"] = self.syncDuration
        map["transferRate"] = nil
        map["isBackground"] = nil
        map["firmware"] = self.firmware
        map["timeOffset"] = nil
        map["syncTime"] = self.syncTime
        map["disconnectReason"] = self.disconnectReason
        map["type"] = self.deviceType.rawValue
        
        return map
    }
}
