import Foundation
import Flutter
import BioIntelliSenseSDK


class DeviceHistoryHandler:BaseHandler {
        
    func onEvent(_ bioEvents: [BioEvent]){
        var map = [String: Any?]()
        var events = [[String: Any?]]()
        bioEvents.forEach{ event in
            events.insert(event.generateMap(), at: 0)
        }
        map["events"] = events
        
        self.eventSink?(map)
    }
    
}

extension BioEvent{
    
    func generateMap()->[String: Any?]{
        var map = [String: Any?]()
        
        map["bioId"] = self.bioId
        map["eventType"] = self.eventType.rawValue
        map["startTime"] = self.startTime
        map["endTime"] = self.endTime
        map["metadata"] = self.metadata?.json
        
        var devices = [[String: Any?]]()
        
        self.scanDevices.forEach{ device in
            devices.insert(device.generateMap(), at: 0)
        }
        map["devices"] = devices
        
        return map
    }
}

extension ScanDevice {
    func generateMap()->[String: Any?]{
        var map = [String: Any?]()
        map["rssi"] = self.rssi
        map["environment"] = self.environment
        map["deviceId"] = self.deviceId
        map["bioId"] = self.bioId
        map["activationState"] = self.activationState.description
        return map
    }
}
