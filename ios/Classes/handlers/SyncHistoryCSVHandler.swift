import Foundation
import Flutter
import BioIntelliSenseSDK

class SyncHistoryCSVHandler:BaseHandler  {
    
    func onEvent(_ bioId: String, csvFileUri: URL?){
        
        var map = [String: Any?]()
        map["bioId"] = bioId
        map["fileUri"] = csvFileUri?.standardizedFileURL
        self.eventSink?(map)
    }

}
