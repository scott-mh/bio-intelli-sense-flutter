import Foundation
import Flutter
import BioIntelliSenseSDK

class ServiceStatusHandler:BaseHandler  {
       
    func onEvent(_ isConnected: Bool){
        self.eventSink?(isConnected)
    }

}
