import Foundation
import Flutter
import BioIntelliSenseSDK

class BioHubIdHandler: BaseHandler  {
    
    func onEvent(_ id: String){
        self.eventSink?(id)
    }

}
