import Foundation
import Flutter
import BioIntelliSenseSDK

class VerifyBioIdHandler: BaseHandler  {
   
    func onEvent(_ statusCode: Int, deviceId: String, deviceType: String){
        
        var map = [String: Any?]()
        map["statusCode"] = statusCode
        map["deviceId"] = deviceId
        map["deviceType"] = deviceType
        self.eventSink?(map)
    }

}
