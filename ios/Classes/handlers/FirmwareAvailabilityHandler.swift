import Foundation
import Flutter
import BioIntelliSenseSDK

class FirmwareAvailabilityHandler:BaseHandler  {
        
    func onEvent(_ bioDevice: BioDevice, isUpdateAvailable: Bool, newPlatformVersion: String){
   
        var map = [String: Any?]()
        map["isUpdateAvailable"] = isUpdateAvailable
        map["newPlatformVersion"] = newPlatformVersion
        map["device"] = bioDevice.generateMap()
    
        self.eventSink?(map)
        
    }

}
