import Foundation
import Flutter
import BioIntelliSenseSDK

class ErrorHandler: BaseHandler  {
        
    func onEvent(_ error: BISError){
        var map = [String: Any?]()
        map["code"] = error.errorCode
        map["message"] = error.description
        self.eventSink?(map)
    }

}
