import Foundation
import Flutter
import BioIntelliSenseSDK

class ConnectionStateHandler: BaseHandler {
        
    func onEvent(_ state: ConnectionState){
        self.eventSink?(state.description)
    }

}
