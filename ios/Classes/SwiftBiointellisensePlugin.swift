import Flutter
import UIKit

public class SwiftBiointellisensePlugin: NSObject, FlutterPlugin {
    
    static var controller: BioIntelliSenseController?
    
    public static func register(with registrar: FlutterPluginRegistrar) {
        
        let channel = FlutterMethodChannel(name: ChannelMethodNames.methodChannelName, binaryMessenger: registrar.messenger())
        
        let instance = SwiftBiointellisensePlugin()
        
        registrar.addMethodCallDelegate(instance, channel: channel)
        
        registerChannels(registrar.messenger())
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
   
        let controller = SwiftBiointellisensePlugin.controller
        let args = call.arguments as? Dictionary<String, Any>
        
        switch call.method{
        
        case ChannelMethodNames.startService:
            controller?.startService(result)
        case ChannelMethodNames.stopService:
            controller?.stopService()
            result(nil)
        case ChannelMethodNames.startScanManual: controller?.startScanManual(result)
        case ChannelMethodNames.stopScan: controller?.stopScan(result)
        case ChannelMethodNames.retrieveQueuedDevices: controller?.retrieveQueuedDevices(result)
        case ChannelMethodNames.retrievePreviouslyConnectedDevices: controller?.retrievePreviouslyConnectedDevices(result)
        case ChannelMethodNames.retrieveDeviceHistory: controller?.retrieveDeviceHistory(args?["bioId"] as! String?, result: result)
        case ChannelMethodNames.retrieveConnectionState: controller?.retrieveConnectionState(result)
        case ChannelMethodNames.retrievePairedDevice: controller?.retrievePairedDevice(result)
        case ChannelMethodNames.setScanMode:
            controller?.setScanMode( args?["mode"] as! String?, result: result)
        case ChannelMethodNames.verifyBioId:
            controller?.verifyBioId( args?["bioId"] as! String?, result: result)
        case ChannelMethodNames.startScanForDevice:
            controller?.startScanForDevice(
            args?["bioId"] as! String?,
            deviceId: args?["deviceId"] as! String?,
            isManual: args?["isManual"] as! Bool?,
            result: result)
        case ChannelMethodNames.retrieveBioHubId: controller?.retrieveBioHubId(result)
        case ChannelMethodNames.retrieveLastSyncTime: controller?.retrieveLastSyncTime(args?["bioId"] as! String?,result: result)
        case ChannelMethodNames.retrieveFirmwarePlatformVersion: controller?.retrieveFirmwarePlatformVersion(result)
        case ChannelMethodNames.retrieveTargetPlatformVersion: controller?.retrieveTargetPlatformVersion(result)
        case ChannelMethodNames.retrieveTargetSDKVersion: controller?.retrieveTargetSDKVersion(result)
        case ChannelMethodNames.retrieveDeviceConfigurationName: controller?.retrieveDeviceConfigurationName(result)
        case ChannelMethodNames.generateCSVHistory: controller?.generateCSVHistory(args?["bioId"] as! String?,result: result)
        case ChannelMethodNames.isScanning:
            controller?.isScanning( result)
        case ChannelMethodNames.enableNotification: controller?.enableNotification(args?["enable"] as! Bool?, result: result)
        case ChannelMethodNames.isNotificationChannelDisabled: controller?.isNotificationChannelDisabled(result)
        case ChannelMethodNames.reset:
            controller?.reset(args?["clearSyncHistory"] as! Bool?, result: result)
        case ChannelMethodNames.syncTimeWithServer:
            controller?.syncTimeWithServer(result)
        default:
            result(FlutterError(code: "999", message: "Unknown Method call", details: nil))
        }
    }
    
    private static func registerChannels(_ messenger: FlutterBinaryMessenger){
        let activationHandler = ActivationHandler()
        let bioHubIdHandler = BioHubIdHandler()
        let deviceHandler = DeviceHandler()
        let serviceStatusHandler = ServiceStatusHandler()
        let syncHistoryCSVHandler = SyncHistoryCSVHandler()
        let verifyBioIdHandler = VerifyBioIdHandler()
        let errorHandler = ErrorHandler()
        let firmwareHandler = FirmwareHandler()
        let lastSyncTimeHandler = LastSyncTimeHandler()
        let scanStateHandler = ScanStateHandler()
        let connectionStateHandler = ConnectionStateHandler()
        let deviceHistoryHandler = DeviceHistoryHandler()
        let firmwareAvailabilityHandler = FirmwareAvailabilityHandler()
        
        FlutterEventChannel(name: ChannelEventNames.activation, binaryMessenger: messenger).setStreamHandler(activationHandler)
        FlutterEventChannel(name: ChannelEventNames.bioHubId, binaryMessenger: messenger).setStreamHandler(bioHubIdHandler)
        FlutterEventChannel(name: ChannelEventNames.device, binaryMessenger: messenger).setStreamHandler(deviceHandler)
        FlutterEventChannel(name: ChannelEventNames.serviceStatus, binaryMessenger: messenger).setStreamHandler(serviceStatusHandler)
        FlutterEventChannel(name: ChannelEventNames.syncHistoryCSV, binaryMessenger: messenger).setStreamHandler(syncHistoryCSVHandler)
        FlutterEventChannel(name: ChannelEventNames.verifyBioId, binaryMessenger: messenger).setStreamHandler(verifyBioIdHandler)
        FlutterEventChannel(name: ChannelEventNames.error, binaryMessenger: messenger).setStreamHandler(errorHandler)
        FlutterEventChannel(name: ChannelEventNames.firmware, binaryMessenger: messenger).setStreamHandler(firmwareHandler)
        FlutterEventChannel(name: ChannelEventNames.lastSyncTime, binaryMessenger: messenger).setStreamHandler(lastSyncTimeHandler)
        FlutterEventChannel(name: ChannelEventNames.scanState, binaryMessenger: messenger).setStreamHandler(scanStateHandler)
        FlutterEventChannel(name: ChannelEventNames.connectionState, binaryMessenger: messenger).setStreamHandler(connectionStateHandler)
        FlutterEventChannel(name: ChannelEventNames.firmwareAvailabilityChannelName, binaryMessenger: messenger).setStreamHandler(firmwareAvailabilityHandler)
        
        controller = BioIntelliSenseController(activationHandler: activationHandler,  bioHubIdHandler: bioHubIdHandler, deviceHandler: deviceHandler, serviceStatusHandler: serviceStatusHandler, syncHistoryCSVHandler: syncHistoryCSVHandler, verifyBioIdHandler: verifyBioIdHandler, errorHandler: errorHandler, firmwareHandler: firmwareHandler, lastSyncTimeHandler: lastSyncTimeHandler, scanStateHandler: scanStateHandler, connectionStateHandler: connectionStateHandler, deviceHistoryHandler: deviceHistoryHandler, firmwareAvailabilityHandler: firmwareAvailabilityHandler)
    }
}
