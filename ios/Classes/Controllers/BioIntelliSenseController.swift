import Foundation
import BioIntelliSenseSDK

class BioIntelliSenseController{
    var bioKit: BioIntelliSenseKit?
    var currentState: BioDeviceState = .disconnected
    
    var activationHandler: ActivationHandler
    var bioHubIdHandler : BioHubIdHandler
    var deviceHandler: DeviceHandler
    var serviceStatusHandler: ServiceStatusHandler
    var syncHistoryCSVHandler: SyncHistoryCSVHandler
    var verifyBioIdHandler : VerifyBioIdHandler
    var errorHandler : ErrorHandler
    var firmwareHandler:FirmwareHandler
    var lastSyncTimeHandler :LastSyncTimeHandler
    var scanStateHandler : ScanStateHandler
    var connectionStateHandler: ConnectionStateHandler
    var deviceHistoryHandler: DeviceHistoryHandler
    var firmwareAvailabilityHandler: FirmwareAvailabilityHandler
    
    init(activationHandler: ActivationHandler,
         bioHubIdHandler : BioHubIdHandler,
         deviceHandler: DeviceHandler,
         serviceStatusHandler: ServiceStatusHandler,
         syncHistoryCSVHandler: SyncHistoryCSVHandler,
         verifyBioIdHandler : VerifyBioIdHandler,
         errorHandler : ErrorHandler,
         firmwareHandler:FirmwareHandler,
         lastSyncTimeHandler :LastSyncTimeHandler,
         scanStateHandler : ScanStateHandler,
         connectionStateHandler: ConnectionStateHandler,
         deviceHistoryHandler: DeviceHistoryHandler,
         firmwareAvailabilityHandler: FirmwareAvailabilityHandler
    ) {
        self.activationHandler = activationHandler
        self.bioHubIdHandler = bioHubIdHandler
        self.deviceHandler = deviceHandler
        self.serviceStatusHandler = serviceStatusHandler
        self.syncHistoryCSVHandler = syncHistoryCSVHandler
        self.verifyBioIdHandler = verifyBioIdHandler
        self.errorHandler = errorHandler
        self.firmwareHandler=firmwareHandler
        self.lastSyncTimeHandler = lastSyncTimeHandler
        self.scanStateHandler = scanStateHandler
        self.connectionStateHandler = connectionStateHandler
        self.deviceHistoryHandler = deviceHistoryHandler
        self.firmwareAvailabilityHandler = firmwareAvailabilityHandler
        
        self.bioKit = BioIntelliSenseKit()
        self.bioKit?.delegate = self
        
        serviceStatusHandler.onEvent(true)
        
    }
    
    func reset(result: @escaping FlutterResult){
        bioKit?.reset()
        result(nil)
    }
    
    func setScanMode(mode: String?){
        if(mode != nil){
            var scanMode = ScanMode.normal
            if(mode?.lowercased() == "pairing"){
                scanMode = .pairing
            }
            self.bioKit?.setScanMode(to: scanMode)
        }
    }
}

extension BioIntelliSenseController: BISDelegate{
    func onConnectionStateChanged(_ state: ConnectionState) {
        connectionStateHandler.onEvent(state)
    }
    
    func onError(_ error: BISError) {
        errorHandler.onEvent(error)
    }
    
    func onBioDeviceDiscovered(bioDevice: BioDevice) {
        
        deviceHandler.onEvent(DeviceEvent.DISCOVERED, device: bioDevice)
    }
    
    func onBioDeviceStateChanged(bioDevice: BioDevice) {
        deviceHandler.onEvent(DeviceEvent.STATE_CHANGE, device: bioDevice)
    }
    
    func onBioSyncProgressChanged(bioDevice: BioDevice) {
        deviceHandler.onEvent(DeviceEvent.SYNC_PROGRESS_CHANGE, device: bioDevice)
    }
    
    func onDeviceActivationSuccessful(_ activationResult: DeviceActivationResult, bioDevice: BioDevice) {
        activationHandler.onEvent(activationResult, bioDevice: bioDevice)
    }
    
    func onDeviceActivationFailed(bioDevice: BioDevice) {
        activationHandler.onFailed(bioDevice)
    }
    
    func onScanStarted() {
        scanStateHandler.onEvent(true)
    }
    
    func onScanStopped() {
        scanStateHandler.onEvent(false)
    }
    
    func onFirmwareUpdateProgressChanged(percentage: Double, bioDevice: BioDevice, firmwareVersion: String) {
        firmwareHandler.onEvent(firmwareVersion, progress: percentage)
        
    }
    
    func onFirmwareUpdateFailed(_ error: BISError, bioDevice: BioDevice) {
        firmwareHandler.onEvent(error.errorCode.rawValue, errorMessage: error.description)
    }
    
    func onFirmwareUpdateSuccessful(bioDevice: BioDevice) {
        firmwareHandler.onEvent(true)
    }
    
    func onFirmwareUpdateAvailability(isUpdateAvailable: Bool, bioDevice: BioDevice, newPlatformVersion: String) {
        firmwareAvailabilityHandler.onEvent(bioDevice, isUpdateAvailable: isUpdateAvailable, newPlatformVersion: newPlatformVersion)
    }
    

}

extension BioIntelliSenseController{
    func startService(_ result: @escaping FlutterResult) {
        //not needed for iOS
        result(nil)
    }
    
    
    func stopService() {
        //not needed for iOS
        
    }
    
    func startScanManual(_ result: @escaping FlutterResult) {
        result(nil)
    }
    
    func startScanForDevice(_ bioId: String?, deviceId: String?, isManual: Bool?, result: @escaping FlutterResult){
        if let id = bioId, let device = deviceId {
            self.bioKit?.startScan(bioId: id, deviceId: device)
            result(nil)
        }else{
            result(FlutterError(code: "003", message: "ScanForDevice BioId and device id can't be null", details: nil))
        }
        
    }
    
    func stopScan(_ result: @escaping FlutterResult) {
        self.bioKit?.stopScan()
        result(nil)
    }
    
    
    func isScanning(_ result: @escaping FlutterResult){
        result(self.bioKit?.isScanning)
    }
    
    func verifyBioId(_ bioId: String?, result: @escaping FlutterResult){
        guard bioId != nil else {
            result(FlutterError(code: "002", message: "VerifyBioId bioId can't be null", details: nil))
            return
        }
        self.bioKit?.verifyBioId(for: bioId!, completion: { statusCode, deviceId, deviceType, error in
            
            if error != nil {
                self.errorHandler.onEvent(error!)
                return
            }
            
            self.verifyBioIdHandler.onEvent(statusCode, deviceId: deviceId, deviceType: deviceType)
        })
        
    }
    
    
    func retrieveQueuedDevices(_ result: @escaping FlutterResult){
        let devices = self.bioKit?.queuedBioDevices
        result(devices)
    }
    
    func retrievePreviouslyConnectedDevices(_ result: @escaping FlutterResult){
        self.bioKit?.previouslyConnectedDevices(completion: { devices in
            result(nil)
        })
        
    }
    
    func retrieveDeviceHistory(_ bioId: String?, result: @escaping FlutterResult) {
        guard bioId != nil else {
            result(FlutterError(code: "001", message: "DeviceHistory: BioId can not be null", details: nil))
            return
        }
        
        self.bioKit?.bioDeviceSyncHistory(for: bioId!, completion: {bioEvents in
            if let events = bioEvents {
                self.deviceHistoryHandler.onEvent(events)
            }
            result(nil)
        })
        
    }
    
    func retrieveConnectionState(_ result: @escaping FlutterResult){
        let state = self.bioKit?.connectionState
        guard state != nil else {
            result(nil)
            return
        }
        connectionStateHandler.onEvent(state!)
        
    }
    
    func retrievePairedDevice(_ result: @escaping FlutterResult){
        guard let device = self.bioKit?.pairedBioDevice else {
            result(nil)
            return
            
        }
        result(device.generateMap())
    }
    
    func retrieveBioHubId(_ result: @escaping FlutterResult){
        self.bioKit?.getBioHubID(completion: { hubId in
            
            self.bioHubIdHandler.onEvent(hubId ?? "")
            
            result(nil)
            
        })
        
    }
    
    func retrieveLastSyncTime(_ bioId: String?, result: @escaping FlutterResult){
        if(bioId != nil){
            self.bioKit?.getLastSyncTime(for: bioId!, completion: { time in
                self.lastSyncTimeHandler.onEvent(time ?? 0)
            })
            result(nil)
        }else{
            result(FlutterError(code: "004", message: "LastSyncTime: BioId can not be null", details: nil))
        }
        
    }
    
    func setScanMode(_ scanMode: String?, result: @escaping FlutterResult){
        var mode = ScanMode.normal
        if(scanMode == "PAIRING"){
            mode = ScanMode.pairing
        }
        self.bioKit?.setScanMode(to: mode)
        result(nil)
    }
    
    func retrieveFirmwarePlatformVersion(_ result: @escaping FlutterResult){
        result(self.bioKit?.firmwarePlatformVersion)
    }
    
    func retrieveTargetPlatformVersion(_ result: @escaping FlutterResult){
        //not available for iOS
        result(nil)
    }
    
    func retrieveTargetSDKVersion(_ result: @escaping FlutterResult){
        result(self.bioKit?.sdkVersion)
    }
    
    func retrieveDeviceConfigurationName(_ result: @escaping FlutterResult){
        result(self.bioKit?.deviceConfigurationName)
    }
    
    func generateCSVHistory(_ bioId: String?, result: @escaping FlutterResult){
        if(bioId != nil){
            self.bioKit?.requestEventHistoryCSV(for: [bioId!], completion: {  URL in
                self.syncHistoryCSVHandler.onEvent(bioId!, csvFileUri: URL)
            })
            result(nil)
        }
        result(FlutterError(code: "005", message: "GenerateCSVHistory: bioId can not be null", details: nil))
    }
    
    func enableNotification(_ enable: Bool?, result: @escaping FlutterResult){
        if(enable != nil){
            self.bioKit?.scheduleLocalNotification(isEnable: enable ?? false)
        }
        result(nil)
    }
    
    func isNotificationChannelDisabled(_ result: @escaping FlutterResult){
        //Not available in iOS
        result(nil)
    }
    
    func reset(_ clearSyncHistory: Bool?,result: @escaping FlutterResult) {
        if(clearSyncHistory != nil){
            self.bioKit?.reset(clearSyncHistory: clearSyncHistory ?? false)
            
        }else{
            self.bioKit?.reset();
        }
        result(nil)
    }
    
    func syncTimeWithServer(_ result: @escaping FlutterResult){
        
        self.bioKit?.syncTimeWithServer()
        result(nil)
        
    }
}

