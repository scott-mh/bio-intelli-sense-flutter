//
//  BioIntelliSenseSDK.h
//  BioIntelliSenseSDK
//
//  Created by Dipen Panchasara on 11/05/20.
//  Copyright © 2020 Infostretch Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for BioIntelliSenseSDK.
FOUNDATION_EXPORT double BioIntelliSenseSDKVersionNumber;

//! Project version string for BioIntelliSenseSDK.
FOUNDATION_EXPORT const unsigned char BioIntelliSenseSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BioIntelliSenseSDK/PublicHeader.h>


