import 'package:flutter/foundation.dart';

enum RouteNames { Main }

extension RouteNamesExtension on RouteNames {
  String get routeNameString => describeEnum(this);
}
