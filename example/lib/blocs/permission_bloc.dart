import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionBloc {
  ValueNotifier<bool> _location = ValueNotifier(false);
  ValueNotifier<bool> _ble = ValueNotifier(false);

  PermissionBloc() {
    Permission.locationWhenInUse.isGranted.then(
      (value) => _location.value = value,
    );
    Permission.bluetooth.isGranted.then(
      (value) => _ble.value = value,
    );
  }

  ValueNotifier<bool> get hasLocationAccess => _location;
  ValueNotifier<bool> get hasBleAccess => _ble;

  Future<bool> requestLocationAlways() async {
    if (await Permission.locationWhenInUse.request().isGranted) {
      _location.value = true;
    } else {
      _location.value = false;
    }
    return _location.value;
  }

  Future<bool> requestBLE() async {
    if (await Permission.bluetooth.request().isGranted) {
      _ble.value = true;
    } else {
      _ble.value = false;
    }
    return _ble.value;
  }
}
