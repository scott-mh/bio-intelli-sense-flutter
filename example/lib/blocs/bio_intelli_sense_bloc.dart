import 'package:biointellisense/BioIntelliSense.dart';
import 'package:biointellisense/models/activation_status.dart';
import 'package:biointellisense/models/bio_device.dart';
import 'package:biointellisense/models/bio_error.dart';
import 'package:biointellisense/models/connection_state.dart';
import 'package:biointellisense/models/device_history.dart';
import 'package:biointellisense/models/scan_mode.dart';

class BioIntelliSenseBloc {
  final _controller = BioIntelliSenseController();

  Stream<ServiceStatus> get serviceStatus => _controller.serviceStatus;
  Stream<ConnState> get connectionState => _controller.connectionState;
  Stream<DeviceEventData> get deviceEventData => _controller.deviceEvents;
  Stream<bool> get scanState => _controller.scanState;
  Stream<BioError> get error => _controller.errors;
  Stream<VerifyBioID> get verifyBioIdResult => _controller.verifyBioIdResult;
  Stream<ActivationStatus> get activation => _controller.activation;
  Stream<Firmware> get firmware => _controller.firmware;
  Stream<String?> get bioHubId => _controller.bioHubId;
  Stream<int?> get lastSyncTime => _controller.lastSyncTime;
  Stream<CSVHistory> get csvHistory => _controller.csvHistory;
  Stream<List<DeviceHistory>> get deviceHistory => _controller.deviceHistory;
  Stream<FirmwareAvailability> get firmwareAvailability =>
      _controller.firmwareAvailability;

  Future<void> startService() async {
    await BioIntelliSenseController.startService();
  }

  Future<void> stopService() async {
    await BioIntelliSenseController.stopService();
  }

  Future<void> startScanManual() async {
    await BioIntelliSenseController.startScanManual();
  }

  Future<void> startScanForDevice(String bioId, String deviceId, bool isManual) async {
    await BioIntelliSenseController.startScanForDevice(
      bioId,
      deviceId,
      isManual: isManual,
    );
  }

  Future<void> stopScan() async {
    await BioIntelliSenseController.stopScan();
  }

  Future<bool> isScanning() async {
    return BioIntelliSenseController.isScanning();
  }

  Future<void> retrieveQueuedDevices() async {
    await BioIntelliSenseController.retrieveQueuedDevices();
  }

  Future<void> retrievePreviouslyConnectedDevices() async {
    await BioIntelliSenseController.retrievePreviouslyConnectedDevices();
  }

  Future<void> retrieveDeviceHistory(String bioId) async {
    await BioIntelliSenseController.retrieveDeviceHistory(bioId);
  }

  Future<ConnState> retrieveConnectionState() async {
    return await BioIntelliSenseController.retrieveConnectionState();
  }

  Future<BioDevice?> retrievePairedDevice() async {
    return await BioIntelliSenseController.retrievePairedDevice();
  }

  Future<void> setScanMode(ScanMode mode) async {
    return await BioIntelliSenseController.setScanMode(mode);
  }

  Future<void> verifyBioId(String bioId) async {
    return await BioIntelliSenseController.verifyBioId(bioId);
  }

  Future<void> retrieveBioHubId() async {
    return await BioIntelliSenseController.retrieveBioHubId();
  }

  Future<void> retrieveLastSyncTime(String bioId) async {
    return await BioIntelliSenseController.retrieveLastSyncTime(bioId);
  }

  Future<String?> retrieveFirmwarePlatformVersion() async {
    return await BioIntelliSenseController.retrieveFirmwarePlatformVersion();
  }

  Future<String?> retrieveTargetPlatformVersion() async {
    return await BioIntelliSenseController.retrieveTargetPlatformVersion();
  }

  Future<String?> retrieveTargetSDKVersion() async {
    return await BioIntelliSenseController.retrieveTargetSDKVersion();
  }

  Future<String?> retrieveDeviceConfigurationName() async {
    return await BioIntelliSenseController.retrieveDeviceConfigurationName();
  }

  Future<void> generateCSVHistory(String bioId) async {
    return await BioIntelliSenseController.generateCSVHistory(bioId);
  }

  Future<void> enableNotification(bool enable) async {
    return await BioIntelliSenseController.enableNotification(enable: enable);
  }

  Future<bool?> isNotificationChannelDisabled() async {
    return await BioIntelliSenseController.isNotificationChannelDisabled();
  }

  Future<void> reset({bool clearSyncHistory = false}) async {
    return await BioIntelliSenseController.reset(clearSyncHistory: clearSyncHistory);
  }

  Future<void> syncTimeWithServer() async {
    return await BioIntelliSenseController.syncTimeWithServer();
  }
}
