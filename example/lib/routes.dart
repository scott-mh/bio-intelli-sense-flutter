import 'package:biointellisense_example/screens/main_screen.dart';

import 'route_names.dart';

final routes = {
  RouteNames.Main.routeNameString: (_) => const MainScreen(),
};
