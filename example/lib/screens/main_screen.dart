import 'dart:async';

import 'package:biointellisense/BioIntelliSense.dart';
import 'package:biointellisense/models/activation_status.dart';
import 'package:biointellisense/models/bio_device.dart';
import 'package:biointellisense/models/device_history.dart';
import 'package:biointellisense_example/blocs/bio_intelli_sense_bloc.dart';
import 'package:biointellisense_example/blocs/permission_bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

const BIO_ID = '2260049719';
const DEVICE_ID = 'm2f004b562f4b3521180f7800';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  final _controller = ScrollController();
  List<String> messages = [];
  StreamSubscription<ServiceStatus>? _statusSubscription;
  StreamSubscription<ConnState>? _connectionSubscription;
  StreamSubscription<DeviceEventData>? _deviceEventSubscription;
  StreamSubscription<bool>? _scanSubscription;
  StreamSubscription<BioError>? _errorSubscription;
  StreamSubscription<VerifyBioID>? _verifySubscription;
  StreamSubscription<ActivationStatus>? _activationSubscription;
  StreamSubscription<Firmware>? _firmwareSubscription;
  StreamSubscription<String?>? _bioHubIdSubscription;
  StreamSubscription<int?>? _lastSyncTimeSubscription;
  StreamSubscription<CSVHistory>? _csvHistorySubscription;
  StreamSubscription<List<DeviceHistory>>? _deviceHistorySubscription;
  StreamSubscription<FirmwareAvailability>? _firmwareAvailabilitySubscription;

  PermissionBloc? _permBloc;
  BioIntelliSenseBloc? _bioBloc;

  String _deviceId = DEVICE_ID;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _permBloc = Provider.of<PermissionBloc>(context, listen: false);
    _bioBloc = Provider.of<BioIntelliSenseBloc>(context, listen: false);

    _permBloc?.hasBleAccess.addListener(() {
      updateMessages('BLE Access: ${_permBloc?.hasBleAccess.value}');
    });
    _permBloc?.hasLocationAccess.addListener(() {
      updateMessages('Location Access: ${_permBloc?.hasLocationAccess.value}');
    });
    _statusSubscription = _bioBloc?.serviceStatus.listen((event) {
      this.updateMessages('Service Status: ${describeEnum(event)}');
    });
    _connectionSubscription = _bioBloc?.connectionState.listen((event) {
      this.updateMessages('Connection State: ${describeEnum(event)}');
    });
    _deviceEventSubscription = _bioBloc?.deviceEventData.listen((event) {
      this.updateMessages('Device Event: ${event.toString()}');
    });
    _scanSubscription = _bioBloc?.scanState.listen((event) {
      this.updateMessages('Scanning: ${event.toString()}');
    });
    _errorSubscription = _bioBloc?.error.listen((event) {
      this.updateMessages('Error: ${event.toString()}');
    });
    _verifySubscription = _bioBloc?.verifyBioIdResult.listen((event) {
      _deviceId = event.deviceId;
      this.updateMessages('verify: ${event.toString()}');
    });
    _activationSubscription = _bioBloc?.activation.listen((event) {
      this.updateMessages('device activation: $event');
    });
    _firmwareSubscription = _bioBloc?.firmware.listen((event) {
      this.updateMessages('firmware: $event');
    });
    _bioHubIdSubscription = _bioBloc?.bioHubId.listen((event) {
      this.updateMessages('Bio Hub Id: $event');
    });
    _lastSyncTimeSubscription = _bioBloc?.lastSyncTime.listen((event) {
      this.updateMessages('Last Sync Time: $event');
    });
    _csvHistorySubscription = _bioBloc?.csvHistory.listen((event) {
      this.updateMessages('CSV History: $event');
    });
    _deviceHistorySubscription = _bioBloc?.deviceHistory.listen((event) {
      event.forEach((history) {
        this.updateMessages('Device History: $history');
      });
    });
    _firmwareAvailabilitySubscription = _bioBloc?.firmwareAvailability.listen((event) {
      this.updateMessages('Firmware Availability: $event');
    });
  }

  @override
  void dispose() {
    _statusSubscription?.cancel();
    _connectionSubscription?.cancel();
    _deviceEventSubscription?.cancel();
    _scanSubscription?.cancel();
    _errorSubscription?.cancel();
    _verifySubscription?.cancel();
    _activationSubscription?.cancel();
    _firmwareSubscription?.cancel();
    _bioHubIdSubscription?.cancel();
    _lastSyncTimeSubscription?.cancel();
    _csvHistorySubscription?.cancel();
    _deviceHistorySubscription?.cancel();
    _firmwareAvailabilitySubscription?.cancel();

    _controller.dispose();

    super.dispose();
  }

  void updateMessages(String message) {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('hh:mm:ss MM-dd-yyyy');
    final String formatted = formatter.format(now);
    messages.add(formatted);
    messages.add(message);
    setState(() {
      messages = messages;
    });
    Timer(
      Duration(seconds: 1),
      () => _controller.jumpTo(_controller.position.maxScrollExtent),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('BioIntelleSense'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: GridView.count(
                padding: EdgeInsets.only(right: 10),
                crossAxisCount: 2,
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                children: [
                  ElevatedButton(
                    onPressed: () => _permBloc?.requestLocationAlways(),
                    child:
                        FittedBox(fit: BoxFit.contain, child: const Text('LOC\nService')),
                  ),
                  ElevatedButton(
                    onPressed: () => _permBloc?.requestBLE(),
                    child:
                        FittedBox(fit: BoxFit.contain, child: const Text('BLE\nService')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Starting BioIntelliSense Service');
                      _bioBloc?.startService();
                    },
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: const Text('Start\nBioIntelliSense\nService')),
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      updateMessages('Retrieve Connection state');
                      final state = await _bioBloc?.retrieveConnectionState();
                      updateMessages('state: $state');
                    },
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: const Text('Retrieve\nConnection\nState')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Stopping BioIntelliSense Service');
                      _bioBloc?.stopService();
                    },
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: const Text('Stop\nBioIntelliSense\nService')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Sync time with Server');
                      _bioBloc?.syncTimeWithServer();
                    },
                    child: FittedBox(
                        fit: BoxFit.contain, child: const Text('Sync Time\nwith Server')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Set Scan mode to Normal');
                      _bioBloc?.setScanMode(ScanMode.NORMAL);
                    },
                    child: FittedBox(
                        fit: BoxFit.contain, child: const Text('Scan Mode:\nNormal')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Set Scan mode to PAIRING');
                      _bioBloc?.setScanMode(ScanMode.PAIRING);
                    },
                    child: FittedBox(
                        fit: BoxFit.contain, child: const Text('Scan Mode:\nPairing')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Start Manual Scan');

                      _bioBloc?.startScanManual();
                    },
                    child: FittedBox(
                        fit: BoxFit.contain, child: const Text('Start Scan\nManual')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Start verify');
                      _bioBloc?.verifyBioId(BIO_ID);
                    },
                    child: FittedBox(
                        fit: BoxFit.contain, child: const Text('Verify Bio ID\n$BIO_ID')),
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      updateMessages('Start scan for device manually');
                      await _bioBloc?.startScanForDevice(BIO_ID, _deviceId, true);
                    },
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: const Text('Start Scan\nManual for\n$BIO_ID')),
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      updateMessages('Start scan for device automatically');
                      await _bioBloc?.startScanForDevice(BIO_ID, _deviceId, false);
                    },
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: const Text('Start Scan\nAuto for\n$BIO_ID')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Retrieve Device History');
                      _bioBloc?.retrieveDeviceHistory(BIO_ID);
                    },
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: const Text('Retrieve \nDevice \nHistory')),
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      updateMessages('Check Is Scanning');
                      final isScanning = await _bioBloc?.isScanning();
                      updateMessages('Is Scanning: $isScanning');
                    },
                    child:
                        FittedBox(fit: BoxFit.contain, child: const Text('Is Scanning')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Stopping scan');
                      _bioBloc?.stopScan();
                    },
                    child: FittedBox(fit: BoxFit.contain, child: const Text('Stop Scan')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Retrieve Queued Devices');
                      _bioBloc?.retrieveQueuedDevices();
                    },
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: const Text('Retrieve\nQueued\nDevices')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Retrieve Previously Connected Devices');
                      _bioBloc?.retrievePreviouslyConnectedDevices();
                    },
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: const Text('Retrieve\nPreviously\nConnected\nDevices')),
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      updateMessages('Retrieve Paired Device');
                      final device = await _bioBloc?.retrievePairedDevice();
                      if (device != null) {
                        updateMessages(device.toString());
                      } else {
                        updateMessages('No Paired Device');
                      }
                    },
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: const Text('Retrieve\nPaired\nDevice')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Retrieving Bio Hub ID');
                      _bioBloc?.retrieveBioHubId();
                    },
                    child: FittedBox(
                        fit: BoxFit.contain, child: const Text('Retrieve Bio \nHub ID')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Retrieve last sync time');
                      _bioBloc?.retrieveLastSyncTime(BIO_ID);
                    },
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: const Text('Retrieve Last\nsync Time \n$BIO_ID')),
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      final version = await _bioBloc?.retrieveFirmwarePlatformVersion();
                      updateMessages('Firware Platform version: $version');
                    },
                    child: FittedBox(
                        fit: BoxFit.contain, child: const Text('Firmware \nVersion')),
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      final version = await _bioBloc?.retrieveTargetPlatformVersion();
                      updateMessages('Target Platform version: $version');
                    },
                    child: FittedBox(
                        fit: BoxFit.contain, child: const Text('Target \nVersion')),
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      final version = await _bioBloc?.retrieveTargetSDKVersion();
                      updateMessages('Target SDK version: $version');
                    },
                    child: FittedBox(
                        fit: BoxFit.contain, child: const Text('SDK \nVersion')),
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      final name = await _bioBloc?.retrieveDeviceConfigurationName();
                      updateMessages('Device Config Name: $name');
                    },
                    child: FittedBox(
                        fit: BoxFit.contain, child: const Text('Device Config \nName')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Generate CSV');
                      _bioBloc?.generateCSVHistory(BIO_ID);
                    },
                    child:
                        FittedBox(fit: BoxFit.contain, child: const Text('Generate CSV')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Enable Notification');
                      _bioBloc?.enableNotification(true);
                    },
                    child: FittedBox(
                        fit: BoxFit.contain, child: const Text('Enable \nNotification')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Disable Notification');
                      _bioBloc?.enableNotification(false);
                    },
                    child: FittedBox(
                        fit: BoxFit.contain, child: const Text('Disable \nNotification')),
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      final status = await _bioBloc?.isNotificationChannelDisabled();
                      updateMessages('Notification Channel Disabled: $status');
                    },
                    child: FittedBox(
                        fit: BoxFit.contain,
                        child: const Text('Is Notification \nDisabled')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Reset');
                      _bioBloc?.reset();
                    },
                    child: FittedBox(fit: BoxFit.contain, child: const Text('Reset')),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      updateMessages('Reset: clear sync history');
                      _bioBloc?.reset(clearSyncHistory: true);
                    },
                    child: FittedBox(
                        fit: BoxFit.contain, child: const Text('Reset & clear')),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 2,
              child: ListView(
                controller: _controller,
                children: messages
                    .map((message) => Padding(
                          padding: const EdgeInsets.only(bottom: 3),
                          child: Text(message),
                        ))
                    .toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
