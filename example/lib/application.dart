import 'package:biointellisense_example/blocs/bio_intelli_sense_bloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'blocs/permission_bloc.dart';
import 'route_names.dart';
import 'routes.dart';

class Application extends StatelessWidget {
  const Application({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(
          create: (_) => PermissionBloc(),
        ),
        Provider(
          create: (_) => BioIntelliSenseBloc(),
        ),
      ],
      child: MaterialApp(
        initialRoute: RouteNames.Main.routeNameString,
        routes: routes,
      ),
    );
  }
}
