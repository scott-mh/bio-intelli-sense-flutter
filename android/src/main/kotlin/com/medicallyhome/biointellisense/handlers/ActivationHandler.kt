package com.medicallyhome.biointellisense.handlers

import com.biointellisense.sdk.enums.DeviceActivationResult
import com.biointellisense.sdk.models.BioDevice


class ActivationHandler: BaseHandler() {
       fun onFailed(bioDevice: BioDevice){
        val map = hashMapOf<String, Any?>()
        map["status"] = "FAILED"
        map["bioId"] = bioDevice.bioId

        eventSink?.success(map)
    }

    fun onEvent(bioDevice: BioDevice, activationResult: DeviceActivationResult){
        val map = hashMapOf<String, Any?>()
        map["status"] = activationResult.name
        map["bioId"] = bioDevice.bioId

        eventSink?.success(map)
    }
}