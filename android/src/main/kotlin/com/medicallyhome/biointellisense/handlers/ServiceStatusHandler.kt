package com.medicallyhome.biointellisense.handlers

class ServiceStatusHandler: BaseHandler() {
    fun onEvent(isConnected: Boolean){
        eventSink?.success(isConnected)
    }
}