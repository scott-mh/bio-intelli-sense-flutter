package com.medicallyhome.biointellisense.handlers

class FirmwareHandler: BaseHandler() {
       fun onEvent(name: String?, progress: Double?){
        onEvent(name, progress, null, null, null)
    }
    fun onEvent(updated: Boolean?){
        onEvent(null, null, updated, null, null)
    }
    fun onEvent(errorCode: Int?, errorMessage: String?){
        onEvent(null, null, null, errorCode, errorMessage)
    }

    private fun onEvent(name: String?, progress: Double?, updated: Boolean?, errorCode: Int?, errorMessage: String?){
        val map = hashMapOf<String, Any?>()
        map["name"] = name
        map["progress"] = progress
        map["updated"] = updated
        map["errorCode"] = errorCode
        map["errorMessage"] = errorMessage

        eventSink?.success(map)
    }
}