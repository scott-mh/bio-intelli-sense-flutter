package com.medicallyhome.biointellisense.handlers

class VerifyBioIdHandler: BaseHandler() {
     fun onEvent( statusCode: Int,
                  deviceId: String,
                  deviceType: String){
        val map = hashMapOf<String, Any?>()
        map["statusCode"] = statusCode
        map["deviceId"] = deviceId
        map["deviceType"] = deviceType
        eventSink?.success(map)
    }
}