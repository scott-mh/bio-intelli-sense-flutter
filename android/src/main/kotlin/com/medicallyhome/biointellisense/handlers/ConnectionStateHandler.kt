package com.medicallyhome.biointellisense.handlers

import com.biointellisense.sdk.enums.ConnectionState

class ConnectionStateHandler: BaseHandler() {

    fun onEvent(newState: ConnectionState?){
        eventSink?.success(newState?.name)
    }
}