package com.medicallyhome.biointellisense.handlers

class ScanStateHandler: BaseHandler() {
     fun onEvent(isScanning: Boolean){
        eventSink?.success(isScanning)
    }
}