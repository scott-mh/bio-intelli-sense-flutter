package com.medicallyhome.biointellisense.handlers

import com.biointellisense.sdk.models.BioDevice

enum class DeviceEvent {
    DISCOVERED,
    STATE_CHANGE,
    SYNC_PROGRESS_CHANGE,
    QUEUED_DEVICE_RECEIVED,
    PREVIOUSLY_CONNECTED_DEVICES_RECEIVED
}

class DeviceHandler: BaseHandler() {

    fun onEvent(event: DeviceEvent, devices: List<BioDevice>){
        val map = hashMapOf<String, Any?>()
        map["event"] = event.name
        map["devices"] = devices.generateMap()
        eventSink?.success(map)
    }

    private fun List<BioDevice>.generateMap(): List<Map<String,Any?>>{
        val list: List<Map<String, Any?>> = ArrayList()

        for (bioDevice in this) {
            (list as MutableList<Map<String, Any?>>).add(bioDevice.generateMap())
        }

        return list
    }

    companion object {
        fun BioDevice.generateMap(): Map<String, Any?> {
            val bioDevice = this

            val map = hashMapOf<String, Any?>()
            map["bioId"] = bioDevice.bioId
            map["state"] = bioDevice.deviceState.name
            map["lastSyncTime"] = bioDevice.lastSyncTime.toDouble()
            map["syncStartTime"] = bioDevice.syncStartTime.toDouble()
            map["syncProgress"] = bioDevice.syncProgress
            map["uploadedBytes"] = bioDevice.uploadedBytes.toDouble()
            map["syncDuration"] = bioDevice.syncDuration.toDouble()
            map["transferRate"] = bioDevice.transferRate.toDouble()
            map["isBackground"] = bioDevice.isBackground
            map["firmware"] = bioDevice.firmware
            map["timeOffset"] = bioDevice.timeOffset?.toDouble()
            map["type"] = bioDevice.deviceType.name


            return map
        }
    }

}