package com.medicallyhome.biointellisense.handlers

class LastSyncTimeHandler: BaseHandler() {

    fun onEvent(time: Long){
        eventSink?.success(time)
    }
}