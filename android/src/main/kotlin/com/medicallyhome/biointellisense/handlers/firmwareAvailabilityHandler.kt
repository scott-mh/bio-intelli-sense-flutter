package com.medicallyhome.biointellisense.handlers

import com.biointellisense.sdk.models.BioDevice
import com.medicallyhome.biointellisense.handlers.DeviceHandler.Companion.generateMap

class FirmwareAvailabilityHandler: BaseHandler() {

    fun onEvent(bioDevice: BioDevice,
                isUpdateAvailable: Boolean,
                newPlatformVersion: String){

        val map = hashMapOf<String, Any?>()

        map["isUpdateAvailable"] = isUpdateAvailable
        map["newPlatformVersion"] = newPlatformVersion
        map["device"] = bioDevice.generateMap()

        eventSink?.success(map)
    }
}