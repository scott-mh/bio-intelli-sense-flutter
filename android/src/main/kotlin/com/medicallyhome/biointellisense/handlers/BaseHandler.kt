package com.medicallyhome.biointellisense.handlers

import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.EventChannel.*

open class BaseHandler: StreamHandler {
    var eventSink: EventChannel.EventSink? = null

    override fun onListen(arguments: Any?, events: EventSink?) {
        eventSink = events
    }

    override fun onCancel(arguments: Any?) {
        eventSink = null
    }
}