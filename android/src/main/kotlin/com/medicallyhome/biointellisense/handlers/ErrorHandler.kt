package com.medicallyhome.biointellisense.handlers

import com.biointellisense.sdk.exceptions.BISError

class ErrorHandler: BaseHandler() {

    fun onEvent(error: BISError){
        val map = hashMapOf<String, Any?>()
        map["code"] = error.errorCode
        map["message"] = error.errorMessage
        eventSink?.success(map)
    }
}