package com.medicallyhome.biointellisense.handlers

import com.biointellisense.sdk.models.BioEvent
import com.biointellisense.sdk.models.ScannedDevice

class DeviceHistoryHandler: BaseHandler() {
    fun onEvent(bioEvents:  List<BioEvent>){
        val events: List<Map<String, Any?>> = ArrayList()
        val map = hashMapOf<String, Any?>()

        for (event in bioEvents) {
            (events as MutableList<Map<String, Any?>>).add(event.generateMap())
        }

        map["events"] = events

        eventSink?.success(map)
    }


    companion object {
        private fun BioEvent.generateMap(): Map<String, Any?> {
            val bioEvent= this

            val map = hashMapOf<String, Any?>()
            map["bioId"] = bioEvent.bioId
            map["eventType"] = bioEvent.eventType.name
            map["startTime"] = bioEvent.startTime.toDouble()
            map["endTime"] = bioEvent.endTime.toDouble()
            map["metadata"] = bioEvent.metadata?.toString()

            val devices: List<Map<String, Any?>> = ArrayList()

            for (device in bioEvent.scannedDevicesList) {
                (devices as MutableList<Map<String, Any?>>).add(device.generateMap())
            }

            map["devices"] = devices
            return map
        }

        private fun ScannedDevice.generateMap(): Map<String, Any?>{
            val device = this
            val map = hashMapOf<String, Any?>()
            map["rssi"] = device.rssi
            map["environment"] = device.environment
            map["deviceId"] = device.deviceId
            map["bioId"] = device.bioId
            map["activationState"] = device.activationState?.name
            return map
        }
    }

}