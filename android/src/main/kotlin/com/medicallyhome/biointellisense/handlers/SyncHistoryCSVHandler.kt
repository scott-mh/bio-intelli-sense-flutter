package com.medicallyhome.biointellisense.handlers

import android.net.Uri

class SyncHistoryCSVHandler: BaseHandler() {

    fun onEvent( bioId: String, csvFileUri: Uri?){
        val map = hashMapOf<String, Any?>()
        map["bioId"] = bioId
        map["fileUri"] = csvFileUri?.path
        eventSink?.success(map)
    }
}