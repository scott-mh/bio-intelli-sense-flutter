package com.medicallyhome.biointellisense.handlers

class BioHubIdHandler: BaseHandler() {

    fun onEvent(id: String){
        eventSink?.success(id)
    }
}