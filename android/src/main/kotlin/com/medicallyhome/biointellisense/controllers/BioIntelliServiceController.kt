package com.medicallyhome.biointellisense.controllers

import android.app.Activity
import android.app.Service
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.net.Uri
import android.os.IBinder
import androidx.annotation.NonNull
import com.biointellisense.sdk.BioIntelliSenseCallback
import com.biointellisense.sdk.BioService
import com.biointellisense.sdk.enums.ConnectionState
import com.biointellisense.sdk.enums.DeviceActivationResult
import com.biointellisense.sdk.enums.ScanMode
import com.biointellisense.sdk.exceptions.BISError
import com.biointellisense.sdk.models.BioDevice
import com.biointellisense.sdk.models.BioEvent
import com.medicallyhome.biointellisense.handlers.*
import com.medicallyhome.biointellisense.handlers.DeviceHandler.Companion.generateMap
import io.flutter.plugin.common.MethodChannel
import java.lang.Exception

class BioIntelliServiceController(
    private val activity: Activity,
    val statusHandler: ServiceStatusHandler,
    val connectionStateHandler: ConnectionStateHandler,
    val deviceHandler: DeviceHandler,
    val scanStateHandler: ScanStateHandler,
    val verifyBioIdHandler: VerifyBioIdHandler,
    val activationHandler: ActivationHandler,
    val firmwareHandler: FirmwareHandler,
    val bioHubIdHandler: BioHubIdHandler,
    val lastSyncTimeHandler: LastSyncTimeHandler,
    val syncHistoryCSVHandler: SyncHistoryCSVHandler,
    val deviceHistoryHandler: DeviceHistoryHandler,
    val firmwareAvailabilityHandler: FirmwareAvailabilityHandler,
    val errorHandler: ErrorHandler
) {

    private var bioServiceBinder: BioService.BioServiceBinder? = null
    private var intent: Intent? = null

    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            bioServiceBinder = service as BioService.BioServiceBinder
            bioServiceBinder?.setBioServiceCallback(bioIntelliSenseCallback)

            statusHandler.onEvent(true)
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            bioServiceBinder = null
            statusHandler.onEvent(false)
        }

    }

    private val bioIntelliSenseCallback = object : BioIntelliSenseCallback {
        override fun onConnectionStateChanged(newState: ConnectionState) {
            connectionStateHandler.onEvent(newState)
        }

        override fun onBioDeviceDiscovered(bioDevice: BioDevice) {
            deviceHandler.onEvent(DeviceEvent.DISCOVERED, listOf(bioDevice))
        }

        override fun onBioDeviceStateChanged(bioDevice: BioDevice) {
            deviceHandler.onEvent(DeviceEvent.STATE_CHANGE, listOf(bioDevice))
        }

        override fun onBioSyncProgressChanged(bioDevice: BioDevice) {
            deviceHandler.onEvent(DeviceEvent.SYNC_PROGRESS_CHANGE, listOf(bioDevice))
        }

        override fun onQueuedBioDevicesReceived(bioDevices: List<BioDevice>) {
            deviceHandler.onEvent(DeviceEvent.QUEUED_DEVICE_RECEIVED, bioDevices)
        }

        override fun onPreviouslyConnectedDevicesReceived(bioDevices: List<BioDevice>) {
            deviceHandler.onEvent(
                DeviceEvent.PREVIOUSLY_CONNECTED_DEVICES_RECEIVED,
                bioDevices
            )
        }

        override fun onDeviceHistoryReceived(deviceHistory: List<BioEvent>) {
            deviceHistoryHandler.onEvent(deviceHistory)
        }

        override fun onScanStarted() {
            scanStateHandler.onEvent(true)
        }

        override fun onScanStopped() {
            scanStateHandler.onEvent(false)
        }

        override fun onDeviceHistoryCSVGenerated(bioIds: List<String>, fileUri: Uri?) {
            if(bioIds.isNotEmpty()) {
                syncHistoryCSVHandler.onEvent(bioIds.first(), fileUri)
            }
        }

        override fun onFirmwareUpdateAvailabilityReceived(
            bioDevice: BioDevice,
            isUpdateAvailable: Boolean,
            newPlatformVersion: String
        ) {
           firmwareAvailabilityHandler.onEvent(bioDevice, isUpdateAvailable, newPlatformVersion)
        }

        override fun onFirmwareUpdateProgressChanged(
            bioDevice: BioDevice,
            firmwareName: String,
            progress: Double
        ) {
            firmwareHandler.onEvent(firmwareName, progress)
        }

        override fun onFirmwareUpdateSuccessful(bioDevice: BioDevice) {
            firmwareHandler.onEvent(true)
        }

        override fun onFirmwareUpdateFailed(bioDevice: BioDevice, error: BISError) {
            firmwareHandler.onEvent(error.errorCode, error.errorMessage)
            errorHandler.onEvent(error)
        }

        override fun onVerifyBioId(
            statusCode: Int,
            deviceId: String,
            deviceType: String,
            error: BISError?
        ) {
            if (error != null) {
                errorHandler.onEvent(error)
            } else {
                verifyBioIdHandler.onEvent(statusCode, deviceId, deviceType)
            }
        }

        override fun onDeviceActivationSuccessful(
            bioDevice: BioDevice,
            activationResult: DeviceActivationResult
        ) {

            activationHandler.onEvent(bioDevice, activationResult)
        }

       override fun onDeviceActivationFailed(bioDevice: BioDevice) {
            activationHandler.onFailed(bioDevice)
        }

        override fun onError(error: BISError) {
            errorHandler.onEvent(error)
        }

        override fun onBioHubIdReceived(bioHubId: Long) {
            bioHubIdHandler.onEvent(bioHubId.toString())
        }
        override fun onLastSyncTimeReceived(lastSyncTime: Long) {
            lastSyncTimeHandler.onEvent(lastSyncTime)
        }

    }

    fun startService(@NonNull result: MethodChannel.Result) {

        intent = Intent(activity, BioService::class.java)
        intent?.putExtra(BioService.EXTRA_AUTO_SCAN, true)
        activity.startService(intent)

        activity.bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE)
        result.success(null)

    }


    fun stopService() {
        activity.unbindService(serviceConnection)
        activity.stopService(intent)
        bioServiceBinder = null
        intent = null
        connectionStateHandler.onEvent(null)
        statusHandler.onEvent(false)

    }

    fun startScanManual(@NonNull result: MethodChannel.Result) {
        bioServiceBinder?.startScan(isManualScan = true)
        result.success(null)
    }

    fun startScanForDevice(
        bioId: String?,
        deviceId: String?,
        isManual: Boolean?,
        @NonNull result: MethodChannel.Result
    ) {

        if (bioId != null && deviceId != null) {
            bioServiceBinder?.startScan(bioId, deviceId, isManual ?: false)
            result.success(null)
        } else {
            result.error("003", "ScanForDevice: bioId and device id can't be null", null)
        }

    }

    fun stopScan(@NonNull result: MethodChannel.Result) {
        bioServiceBinder?.stopScan()
        result.success(null)
    }


    fun isScanning(@NonNull result: MethodChannel.Result) {
        result.success(bioServiceBinder?.isScanning())
    }

    fun verifyBioId(bioId: String?, @NonNull result: MethodChannel.Result) {

        if (bioId != null) {
            bioServiceBinder?.verifyBioId(bioId)
            result.success(null)
        } else {
            result.error("002", "VerifyBioId: bioId can't be null", null)
        }

    }


    fun retrieveQueuedDevices(@NonNull result: MethodChannel.Result) {
        bioServiceBinder?.getQueuedBioDevices()
        result.success(null)
    }

    fun retrievePreviouslyConnectedDevices(@NonNull result: MethodChannel.Result) {
        bioServiceBinder?.getPreviouslyConnectedDevices()
        result.success(null)
    }

    fun retrieveDeviceHistory(bioId: String?, @NonNull result: MethodChannel.Result) {

        if(bioId != null){
            bioServiceBinder?.getDeviceHistory(bioId)
            result.success(null)
            return
        }
        result.error("001", "retrieveDeviceHistory: bioId can not be null", null)

    }

    fun retrieveConnectionState(@NonNull result: MethodChannel.Result) {
        result.success(bioServiceBinder?.getConnectionState()?.name)
    }

    fun retrievePairedDevice(@NonNull result: MethodChannel.Result) {
        result.success(bioServiceBinder?.getPairedDevice()?.generateMap())
    }

    fun retrieveBioHubId(@NonNull result: MethodChannel.Result) {
        bioServiceBinder?.getBioHubId()
        result.success(null)
    }

    fun retrieveLastSyncTime(bioId: String?, @NonNull result: MethodChannel.Result) {

        if (bioId != null) {
            bioServiceBinder?.getLastSyncTime(bioId)
            result.success(null)
        } else {
            result.error("004", "LastSyncTime: bioId can not be null", null)
        }

    }

    fun setScanMode(scanMode: String?, @NonNull result: MethodChannel.Result) {

        var mode = ScanMode.NORMAL
        if (scanMode == "PAIRING") {
            mode = ScanMode.PAIRING
        }
        bioServiceBinder?.setScanMode(mode)
        result.success(null)

    }

    fun retrieveFirmwarePlatformVersion(@NonNull result: MethodChannel.Result) {
        result.success(bioServiceBinder?.getFirmwarePlatformVersion())
    }

    fun retrieveTargetPlatformVersion(@NonNull result: MethodChannel.Result) {
        result.success(bioServiceBinder?.getTargetPlatformVersion())
    }

    fun retrieveTargetSDKVersion(@NonNull result: MethodChannel.Result) {
        result.success(bioServiceBinder?.getTargetSDKVersion())
    }

    fun retrieveDeviceConfigurationName(@NonNull result: MethodChannel.Result) {
        result.success(bioServiceBinder?.getDeviceConfigurationName())
    }

    fun generateCSVHistory(bioId: String?, @NonNull result: MethodChannel.Result) {

        try {
            if (bioId != null) {
                bioServiceBinder?.generateDeviceHistoryCSV(listOf(bioId))
                result.success(null)
                return
            }
            result.error("005", "generateCSVHistory: bioId can not be null", null)
        }catch(exception: Exception){
            result.error("100", "generateCSVHistory: exception", exception)
        }

    }

    fun enableNotification(enable: Boolean?, @NonNull result: MethodChannel.Result) {
        bioServiceBinder?.enableNotification(enable ?: false)
        result.success(null)
    }

    fun isNotificationChannelDisabled(@NonNull result: MethodChannel.Result) {
        result.success(bioServiceBinder?.isNotificationChannelDisabled())
    }

    fun reset(clearSyncHistory: Boolean?, @NonNull result: MethodChannel.Result) {
        bioServiceBinder?.reset(clearSyncHistory ?: false)
        result.success(null)
    }

    fun syncTimeWithServer(@NonNull result: MethodChannel.Result) {
        bioServiceBinder?.syncTimeWithServer()
        result.success(null)
    }
}