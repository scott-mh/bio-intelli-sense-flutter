package com.medicallyhome.biointellisense

import android.app.Activity
import androidx.annotation.NonNull
import com.medicallyhome.biointellisense.controllers.BioIntelliServiceController
import com.medicallyhome.biointellisense.handlers.*

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result


class BioIntelliSensePlugin : FlutterPlugin, MethodCallHandler, ActivityAware {
    private lateinit var methodChannel: MethodChannel

    private lateinit var serviceStatusChannel: EventChannel
    private lateinit var connectionStateChannel: EventChannel
    private lateinit var deviceChannel: EventChannel
    private lateinit var scanStateChannel: EventChannel
    private lateinit var errorChannel: EventChannel
    private lateinit var verifyBioIdChannel: EventChannel
    private lateinit var activationChannel: EventChannel
    private lateinit var firmwareChannel: EventChannel
    private lateinit var bioHubIdChannel: EventChannel
    private lateinit var lastSyncTimeChannel: EventChannel
    private lateinit var syncHistoryCSVChannel: EventChannel
    private lateinit var deviceHistoryChannel: EventChannel
    private lateinit var firmwareAvailabilityChannel: EventChannel

    private lateinit var controller: BioIntelliServiceController
    private lateinit var binaryMessenger: BinaryMessenger

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        binaryMessenger = flutterPluginBinding.binaryMessenger
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        when (call.method) {
            startServiceMethod -> controller.startService(result)
            stopServiceMethod -> {controller.stopService(); result.success(null)}
            startScanManualMethod -> controller.startScanManual(result)
            stopScanMethod -> controller.stopScan(result)
            retrieveQueuedDevicesMethod -> controller.retrieveQueuedDevices(result)
            retrievePreviouslyConnectedDevicesMethod -> controller.retrievePreviouslyConnectedDevices(result)
            retrieveDeviceHistoryMethod -> controller.retrieveDeviceHistory(call.argument<String>("bioId"), result)
            retrieveConnectionStateMethod -> controller.retrieveConnectionState(result)
            retrievePairedDeviceMethod -> controller.retrievePairedDevice(result)
            setScanModeMethod -> controller.setScanMode(call.argument<String>("mode"), result)
            verifyBioIdMethod -> controller.verifyBioId(call.argument<String>("bioId"), result)
            startScanForDeviceMethod -> controller.startScanForDevice(
                call.argument<String>("bioId"),
                call.argument<String>("deviceId"),
                call.argument<Boolean>("isManual"),
                result)
            retrieveBioHubIdMethod -> controller.retrieveBioHubId(result)
            retrieveLastSyncTimeMethod -> controller.retrieveLastSyncTime(call.argument<String>("bioId"),result)
            retrieveFirmwarePlatformVersionMethod -> controller.retrieveFirmwarePlatformVersion(result)
            retrieveTargetPlatformVersionMethod -> controller.retrieveTargetPlatformVersion(result)
            retrieveTargetSDKVersionMethod -> controller.retrieveTargetSDKVersion(result)
            retrieveDeviceConfigurationNameMethod -> controller.retrieveDeviceConfigurationName(result)
            generateCSVHistoryMethod -> controller.generateCSVHistory(call.argument<String>("bioId"), result)
            isScanningMethod -> controller.isScanning(result)
            enableNotificationMethod -> controller.enableNotification(call.argument<Boolean>("enable"), result)
            isNotificationChannelDisabledMethod -> controller.isNotificationChannelDisabled(result)
            resetMethod -> controller.reset(call.argument<Boolean>("clearSyncHistory"), result)
            syncTimeWithServerMethod -> controller.syncTimeWithServer(result)

        }

    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        methodChannel.setMethodCallHandler(null)
        serviceStatusChannel.setStreamHandler(null)
        connectionStateChannel.setStreamHandler(null)
        deviceChannel.setStreamHandler(null)
        scanStateChannel.setStreamHandler(null)
        errorChannel.setStreamHandler(null)
        verifyBioIdChannel.setStreamHandler(null)
        activationChannel.setStreamHandler(null)
        firmwareChannel.setStreamHandler(null)
        bioHubIdChannel.setStreamHandler(null)
        lastSyncTimeChannel.setStreamHandler(null)
        syncHistoryCSVChannel.setStreamHandler(null)
        deviceHistoryChannel.setStreamHandler(null)
        firmwareAvailabilityChannel.setStreamHandler(null)
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        setupChannels(binding.activity)
    }

    override fun onDetachedFromActivityForConfigChanges() {}

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {}

    override fun onDetachedFromActivity() {
        controller.stopService()
    }

    private fun setupChannels(activity: Activity) {
        val serviceConnectionHandler = ServiceStatusHandler()
        serviceStatusChannel = EventChannel(binaryMessenger, serviceStatusChannelName)
        serviceStatusChannel.setStreamHandler(serviceConnectionHandler)

        val connectionStateHandler = ConnectionStateHandler()
        connectionStateChannel = EventChannel(binaryMessenger, connectionStateChannelName)
        connectionStateChannel.setStreamHandler(connectionStateHandler)

        val deviceHandler = DeviceHandler()
        deviceChannel = EventChannel(binaryMessenger, deviceChannelName)
        deviceChannel.setStreamHandler(deviceHandler)

        val scanStateHandler = ScanStateHandler()
        scanStateChannel = EventChannel(binaryMessenger, scanStateChannelName)
        scanStateChannel.setStreamHandler(scanStateHandler)

        val verifyBioIdHandler = VerifyBioIdHandler()
        verifyBioIdChannel = EventChannel(binaryMessenger, verifyBioIdChannelName)
        verifyBioIdChannel.setStreamHandler(verifyBioIdHandler)

        val activationHandler = ActivationHandler()
        activationChannel = EventChannel(binaryMessenger, activationChannelName)
        activationChannel.setStreamHandler(activationHandler)

        val firmwareHandler = FirmwareHandler()
        firmwareChannel = EventChannel(binaryMessenger, firmwareChannelName)
        firmwareChannel.setStreamHandler(firmwareHandler)

        val bioHubIdHandler = BioHubIdHandler()
        bioHubIdChannel = EventChannel(binaryMessenger, bioHubIdChannelName)
        bioHubIdChannel.setStreamHandler(bioHubIdHandler)

        val syncHistoryCSVHandler = SyncHistoryCSVHandler()
        syncHistoryCSVChannel = EventChannel(binaryMessenger, syncHistoryCSVChannelName)
        syncHistoryCSVChannel.setStreamHandler(syncHistoryCSVHandler)

        val lastSyncTimeHandler = LastSyncTimeHandler()
        lastSyncTimeChannel = EventChannel(binaryMessenger, lastSyncTimeChannelName)
        lastSyncTimeChannel.setStreamHandler(lastSyncTimeHandler)

        val deviceHistoryHandler = DeviceHistoryHandler()
        deviceHistoryChannel = EventChannel(binaryMessenger, deviceHistoryChannelName)
        deviceHistoryChannel.setStreamHandler(deviceHistoryHandler)

        val firmwareAvailabilityHandler = FirmwareAvailabilityHandler()
        firmwareAvailabilityChannel = EventChannel(binaryMessenger, firmwareAvailabilityChannelName)
        firmwareAvailabilityChannel.setStreamHandler(firmwareAvailabilityHandler)

        val errorHandler = ErrorHandler()
        errorChannel = EventChannel(binaryMessenger, errorChannelName)
        errorChannel.setStreamHandler(errorHandler)

        controller =
            BioIntelliServiceController(
                activity,
                serviceConnectionHandler,
                connectionStateHandler,
                deviceHandler,
                scanStateHandler,
                verifyBioIdHandler,
                activationHandler,
                firmwareHandler,
                bioHubIdHandler,
                lastSyncTimeHandler,
                syncHistoryCSVHandler,
                deviceHistoryHandler,
                firmwareAvailabilityHandler,
                errorHandler)

        methodChannel = MethodChannel(binaryMessenger, methodChannelName)
        methodChannel.setMethodCallHandler(this)

    }

    private companion object {
        private const val methodChannelName = "BioIntelliSense"
        private const val serviceStatusChannelName = "BioIntelliSense/ServiceStatus"
        private const val connectionStateChannelName = "BioIntelliSense/ConnectionState"
        private const val scanStateChannelName = "BioIntelliSense/ScanState"
        private const val deviceChannelName = "BioIntelliSense/device"
        private const val errorChannelName = "BioIntelliSense/error"
        private const val verifyBioIdChannelName = "BioIntelliSense/verifyBioId"
        private const val activationChannelName = "BioIntelliSense/activation"
        private const val firmwareChannelName = "BioIntelliSense/firmware"
        private const val bioHubIdChannelName = "BioIntelliSense/bioHubId"
        private const val lastSyncTimeChannelName = "BioIntelliSense/lastSyncTime"
        private const val syncHistoryCSVChannelName = "BioIntelliSense/syncHistoryCSV"
        private const val deviceHistoryChannelName = "BioIntelliSense/deviceHistory"
        private const val firmwareAvailabilityChannelName = "BioIntelliSense/firmwareAvailability"

        private const val startServiceMethod = "startService"
        private const val stopServiceMethod = "stopService"
        private const val startScanManualMethod = "startScanManual"
        private const val stopScanMethod = "stopScan"
        private const val retrieveQueuedDevicesMethod = "retrieveQueuedDevices"
        private const val retrievePreviouslyConnectedDevicesMethod = "retrievePreviouslyConnectedDevices"
        private const val retrieveDeviceHistoryMethod = "retrieveDeviceHistory"
        private const val retrieveConnectionStateMethod = "retrieveConnectionState"
        private const val retrievePairedDeviceMethod = "retrievePairedDevice"
        private const val setScanModeMethod = "setScanMode"
        private const val verifyBioIdMethod = "verifyBioId"
        private const val startScanForDeviceMethod = "startScanForDevice"
        private const val retrieveBioHubIdMethod = "retrieveBioHubId"
        private const val retrieveLastSyncTimeMethod = "retrieveLastSyncTime"
        private const val retrieveFirmwarePlatformVersionMethod = "retrieveFirmwarePlatformVersion"
        private const val retrieveTargetPlatformVersionMethod = "retrieveTargetPlatformVersion"
        private const val retrieveTargetSDKVersionMethod = "retrieveTargetSDKVersion"
        private const val retrieveDeviceConfigurationNameMethod = "retrieveDeviceConfigurationName"
        private const val generateCSVHistoryMethod = "generateCSVHistory"
        private const val isScanningMethod = "isScanning"
        private const val enableNotificationMethod = "enableNotification"
        private const val isNotificationChannelDisabledMethod = "isNotificationChannelDisabled"
        private const val resetMethod = "reset"
        private const val syncTimeWithServerMethod = "syncTimeWithServer"
    }
}
