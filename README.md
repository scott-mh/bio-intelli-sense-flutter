# BioIntelliSense

[The BioIntelliSense Device](https://biointellisense.com) is a device that is attached to the patients upper chest area and provides monitoring of key vitals

- Skin Temperature
- Heart Rate
- Respiratory Rate
- Body Position
- Activity level
- Coughing Frequency

There are two types of devices:

- BioSticker
- BioButton

This plugin was tested with the BioSticker but should also support the BioButton

## Overview

The Plugin is a wrapper for the BioIntelliSense native SDKs.

- The BioIntelliSenseSDK encapsulates all the BLE functionality for the device.
- The SDK provides interfaces to execute tasks within the SDK
- The SDK provides callbacks when events occur
- All vital data is encrypted and the SDK handles posting to the BioCloud

To help with testing, the Plugin provides an example App that exposes all the methods and callbacks to Flutter.

## Setup

There are 2 versions of the SDK.

One is for the Staging environment and one is for the production environment.

Currently there is no way to configure (via settings) which environment the SDK communicates with.

The Example App is using the SDK that points to the Staging environment

The 1.3 version SDKs (iOS and Android) are located in the SDKs folder of this repo

The BioID is the id that is assigned to the device. The id is located on the shipping package and is used to activate and scan for the device.

## Testing

To test the plugin, you'll need to have a Bio Device (Button or sticker)
To have the vitals data posted to the cloud, an order must be sent to the BioCloud.

In the documents/postman folder is a post man collection that has an order post
Before using, you will need to include the Authorization API key.

Staging keys are located on the [Medically Home SharePoint site](https://medicallyhomegroup.sharepoint.com/sites/MHGEngineering/Technical%20Documents/Forms/AllItems.aspx?FolderCTID=0x0120004AEF1A0556F8E94495C8EC2982F9C70F&viewid=665a69a6%2D0b6b%2D4240%2D96cc%2De640b0651a17&id=%2Fsites%2FMHGEngineering%2FTechnical%20Documents%2FSDKs%2FBioIntellisense%2FKeys)

Once the order is placed, you can utilize the device with the sample application.

In the Example application

- Edit the /lib/screens/main_screen.dart file
- line 14 add the 10 digit bioId (no dashes) for your device
- Launch the Example App
- Start the BioIntelliSense service
- Set Scan mode to Pairing
- Verify the Bio Id
  - verifying will retrieve the device ID and store it in a variable
  - This is needed to Start a Scan for the device
  - if the App is stopped, you will need to verify again to get the device id
  - To prevent this, add the DeviceId to line 15 of the main_screen.dart

The SDK will auto scan for the device every 15 min. You can force a scan by clicking the "Start Scan manual for bio id"

## Documentation

Documentation for the SDK can be found in /documents folder of this repo

- TechOverview.pdf: provides an high level overview of the product
- AndroidSDK1.3.pdf
- iOSSDK1.3.pdf
- OrderApi.pdf:
- ./postman/BioIntelliSense.postman_collection.json
