import 'package:biointellisense/models/bio_device.dart';

class Firmware {
  Firmware({
    this.name,
    this.progress,
    this.updated,
    this.errorCode,
    this.errorMessage,
  });

  factory Firmware.fromMap(Map<Object?, Object?> map) => Firmware(
        name: map['name'] as String?,
        progress: map['progress'] as double?,
        updated: map['updated'] as bool?,
        errorCode: map['errorCode'] as int?,
        errorMessage: map['errorMessage'] as String?,
      );

  final String? name;
  final double? progress;
  final bool? updated;
  final int? errorCode;
  final String? errorMessage;

  @override
  String toString() {
    return 'Firmware: name: $name progress: $progress updated: $updated errorCode: $errorCode errorMsg: $errorMessage';
  }
}

class FirmwareAvailability {
  FirmwareAvailability({
    required this.isUpdateAvailable,
    required this.newPlatformVersion,
    required this.device,
  });

  factory FirmwareAvailability.fromMap(Map<Object?, Object?> map) => FirmwareAvailability(
        isUpdateAvailable: map['name'] as bool,
        newPlatformVersion: map['progress'] as String,
        device: BioDevice.fromMap(map['device'] as Map<Object?, Object?>),
      );

  final bool isUpdateAvailable;
  final String newPlatformVersion;
  final BioDevice device;

  @override
  String toString() {
    return 'Firmware Availability: bioId: ${device.bioId} isUpdateAvailable: $isUpdateAvailable newPlatformVersion: $newPlatformVersion';
  }
}
