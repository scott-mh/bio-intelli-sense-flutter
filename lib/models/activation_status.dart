class ActivationStatus {
  ActivationStatus({
    required this.status,
    required this.bioId,
  });

  final String status;
  final String bioId;

  factory ActivationStatus.fromMap(Map<Object?, Object?> map) => ActivationStatus(
        bioId: map['bioId'] as String,
        status: map['status'] as String,
      );

  @override
  String toString() {
    return 'Activation: status $status, bioId: $bioId';
  }
}
