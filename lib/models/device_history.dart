class DeviceHistory {
  DeviceHistory({
    required this.bioId,
    required this.eventType,
    this.startTime,
    this.endTime,
    this.metaData,
    required this.devices,
  });

  final String? bioId;
  final String? eventType;
  final double? startTime;
  final double? endTime;
  final String? metaData;
  final List<ScannedDevice>? devices;

  factory DeviceHistory.fromMap(Map<Object?, Object?> map) => DeviceHistory(
        bioId: map["bioId"] as String?,
        eventType: map["eventType"] as String?,
        startTime: map["startTime"] as double?,
        endTime: map["endTime"] as double?,
        metaData: map["metaData"] as String?,
        devices: retrieveDevices(
          map["devices"] as List<Object?>,
        ),
      );

  static List<ScannedDevice> retrieveDevices(List<Object?> list) {
    final devices = <ScannedDevice>[];
    list.forEach((element) {
      devices.add(ScannedDevice.fromMap(element as Map<Object?, Object?>));
    });
    return devices;
  }

  @override
  String toString() {
    String deviceList = 'Devices: \n';
    devices?.forEach((device) {
      deviceList += '${device.toString()}\n';
    });
    return 'bioId: $bioId, eventType: $eventType, startTime: $startTime, endTime: $endTime, metaData: $metaData \n$deviceList';
  }
}

class ScannedDevice {
  ScannedDevice({
    required this.rssi,
    required this.deviceId,
    required this.bioId,
    this.environment,
    this.activationState,
  });

  final int rssi;
  final String? environment;
  final String deviceId;
  final String bioId;
  final String? activationState;

  factory ScannedDevice.fromMap(Map<Object?, Object?> map) => ScannedDevice(
        rssi: map["rssi"] as int,
        environment: (map["environment"] as int?) == 2 ? 'Staging' : 'Production',
        deviceId: map["deviceId"] as String,
        bioId: map['bioId'] as String,
        activationState: map["activationState"] as String?,
      );

  @override
  String toString() {
    return 'rssi: $rssi, environment: $environment, deviceId: $deviceId, bioId: $bioId, activationStatie: $activationState';
  }
}
