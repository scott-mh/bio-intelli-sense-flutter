enum ServiceStatus {
  NotStarted,
  Started,
  Stopped,
}

ServiceStatus mapToServiceStatus(Object? event) {
  if (event == null) {
    return ServiceStatus.NotStarted;
  } else if (event == true) {
    return ServiceStatus.Started;
  }
  return ServiceStatus.Stopped;
}
