import 'package:flutter/foundation.dart';

enum DeviceType {
  BUTTON,
  STICKER,
  UNKNOWN,
}

DeviceType mapToDeviceType(Object? event) {
  if (event == null) {
    return DeviceType.UNKNOWN;
  }
  final name = event.toString().toUpperCase();

  if (name == 'BUTTON') {
    return DeviceType.BUTTON;
  } else if (name == 'STICKER') {
    return DeviceType.STICKER;
  } else if (name == 'BIOSTICKER') {
    return DeviceType.STICKER;
  } else if (name == 'BIOBUTTON') {
    return DeviceType.BUTTON;
  }

  return DeviceType.UNKNOWN;
}

enum DeviceEvent {
  UNKNOWN,
  DISCOVERED,
  STATE_CHANGE,
  SYNC_PROGRESS_CHANGE,
  QUEUED_DEVICE_RECEIVED,
  PREVIOUSLY_CONNECTED_DEVICES_RECEIVED,
  SYNC_HISTORY_RECEIVED
}

DeviceEvent mapToDeviceEvent(Object? event) {
  if (event == null) {
    return DeviceEvent.UNKNOWN;
  }
  final name = event.toString().toUpperCase();

  switch (name) {
    case 'DISCOVERED':
      return DeviceEvent.DISCOVERED;
    case 'STATE_CHANGE':
      return DeviceEvent.STATE_CHANGE;
    case 'SYNC_PROGRESS_CHANGE':
      return DeviceEvent.SYNC_PROGRESS_CHANGE;
    case 'QUEUED_DEVICE_RECEIVED':
      return DeviceEvent.QUEUED_DEVICE_RECEIVED;
    case 'PREVIOUSLY_CONNECTED_DEVICES_RECEIVED':
      return DeviceEvent.PREVIOUSLY_CONNECTED_DEVICES_RECEIVED;
    case 'SYNC_HISTORY_RECEIVED':
      return DeviceEvent.SYNC_HISTORY_RECEIVED;
    default:
      return DeviceEvent.UNKNOWN;
  }
}

enum DeviceState {
  UNKNOWN,
  WAITING,
  CONNECTING,
  FIRMWARE_UPDATE_CHECKING,
  FIRMWARE_DOWNLOADING,
  FIRMWARE_INSTALLING,
  FIRMWARE_VERIFICATION,
  CHECKING_CONFIGURATION,
  CONFIGURING_DEVICE,
  CONNECTED,
  SYNCING,
  DISCONNECTED,
}

DeviceState mapToDeviceState(Object? event) {
  if (event == null) {
    return DeviceState.UNKNOWN;
  }
  final name = event.toString().toUpperCase();

  switch (name) {
    case 'UNKNOWN':
      return DeviceState.UNKNOWN;
    case 'WAITING':
      return DeviceState.WAITING;
    case 'CONNECTING':
      return DeviceState.CONNECTING;
    case 'FIRMWARE_UPDATE_CHECKING':
      return DeviceState.FIRMWARE_UPDATE_CHECKING;
    case 'FIRMWARE_DOWNLOADING':
      return DeviceState.FIRMWARE_DOWNLOADING;
    case 'FIRMWARE_INSTALLING':
      return DeviceState.FIRMWARE_INSTALLING;
    case 'FIRMWARE_VERIFICATION':
      return DeviceState.FIRMWARE_VERIFICATION;
    case 'CHECKING_CONFIGURATION':
      return DeviceState.CHECKING_CONFIGURATION;
    case 'CONFIGURING_DEVICE':
      return DeviceState.CONFIGURING_DEVICE;
    case 'CONNECTED':
      return DeviceState.CONNECTED;
    case 'SYNCING':
      return DeviceState.SYNCING;
    case 'DISCONNECTED':
      return DeviceState.DISCONNECTED;
    default:
      return DeviceState.UNKNOWN;
  }
}

class BioDevice {
  BioDevice(
      {required this.bioId,
      required this.type,
      required this.state,
      this.lastSyncTime,
      this.syncStartTime,
      this.syncProgress,
      this.uploadedBytes,
      this.syncDuration,
      this.transferRate,
      this.isBackground,
      this.firmware,
      this.timeOffset,
      this.disconnectReason,
      this.syncTime});

  factory BioDevice.fromMap(Map<Object?, Object?> map) => BioDevice(
        bioId: map['bioId'] as String,
        type: mapToDeviceType(map['type'] as String),
        state: mapToDeviceState(map['state'] as String),
        lastSyncTime: map['lastSyncTime'] as double?,
        syncStartTime: map['syncStartTime'] as double?,
        syncProgress: map['syncProgress'] as double,
        uploadedBytes: map['uploadedBytes'] as double?,
        syncDuration: map['syncDuration'] as double?,
        transferRate: map['transferRate'] as double?,
        isBackground: map['isBackground'] as bool?,
        firmware: map['firmware'] as String?,
        timeOffset: map['timeOffset'] as double?,
        disconnectReason: map['disconnectReason'] as int?,
        syncTime: map["syncTime"] as double?,
      );

  final String bioId;
  final DeviceType type;
  final DeviceState state;
  final double? lastSyncTime;
  final double? syncStartTime;
  final double? syncProgress;
  final double? uploadedBytes;
  final double? syncDuration;
  final double? transferRate;
  final bool? isBackground;
  final String? firmware;
  final double? timeOffset;
  final double? syncTime;
  final int? disconnectReason;

  @override
  String toString() {
    return '''
    BioDevice: $bioId, 
    type: ${describeEnum(type)}, 
    state: ${describeEnum(state)}, 
    lastSyncType: $lastSyncTime, 
    syncStartTime: $syncStartTime, 
    syncProgress: $syncProgress, 
    uploadedBytes: $uploadedBytes, 
    syncDuration: $syncDuration, 
    transferRate: $transferRate, 
    isBackground: $isBackground, 
    firmware: $firmware 
    timeOffset: $timeOffset,
    syncTime: $syncTime,
    disconnectReason: $disconnectReason
    ''';
  }
}

class DeviceEventData {
  DeviceEventData({
    required this.event,
    required this.devices,
  });

  final DeviceEvent event;
  final List<BioDevice> devices;

  factory DeviceEventData.fromMap(Map<Object?, Object?> map) => DeviceEventData(
        event: mapToDeviceEvent(map['event'] as String),
        devices: (map['devices'] as List<Object?>)
            .map((map) => BioDevice.fromMap(map as Map<Object?, Object?>))
            .toList(),
      );

  @override
  String toString() {
    return 'BioDeviceEvent: ${describeEnum(this.event)}, \ndevices: ${this.devices.map((e) => e.toString())}, ';
  }
}

class VerifyBioID {
  VerifyBioID({
    required this.statusCode,
    required this.deviceId,
    required this.deviceType,
  });

  final int statusCode;
  final String deviceId;
  final DeviceType deviceType;

  factory VerifyBioID.fromMap(Map<Object?, Object?> map) => VerifyBioID(
        statusCode: map['statusCode'] as int,
        deviceId: map['deviceId'] as String,
        deviceType: mapToDeviceType(map['deviceType'] as String),
      );

  @override
  String toString() {
    return 'VerifyBioID: code: $statusCode deviceId: $deviceId deviceType: ${describeEnum(deviceType)}';
  }
}
