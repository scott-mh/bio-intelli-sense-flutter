enum ScanMode {
  UNKNOWN,
  PAIRING,
  NORMAL,
}

ScanMode mapToScanMode(Object? event) {
  if (event == 'PAIRING') {
    return ScanMode.PAIRING;
  } else if (event == 'NORMAL') {
    return ScanMode.NORMAL;
  }
  return ScanMode.UNKNOWN;
}
