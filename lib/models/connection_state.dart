enum ConnState {
  UNKNOWN,
  INITIALIZING,
  READY,
  BLUETOOTH_OFF,
  INTERNET_OFF,
  INITIALISING,
  UNAUTHORISED,
  TIME_SYNC_FAILED
}

ConnState mapToConnectionState(Object? event) {
  if (event == null) {
    return ConnState.UNKNOWN;
  }
  final name = event.toString().toUpperCase();
  switch (name) {
    case 'INITIALIZING':
      return ConnState.INITIALIZING;
    case 'READY':
      return ConnState.READY;
    case 'BLUETOOTH_OFF':
      return ConnState.BLUETOOTH_OFF;
    case 'INTERNET_OFF':
      return ConnState.INTERNET_OFF;
    case 'UNAUTHORISED':
      return ConnState.UNAUTHORISED;
    case 'TIME_SYNC_FAILED':
      return ConnState.TIME_SYNC_FAILED;
    default:
      return ConnState.UNKNOWN;
  }
}
