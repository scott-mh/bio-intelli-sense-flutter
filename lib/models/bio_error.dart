enum ErrorType {
  UNKNOWN_ERROR,
  INVALID_BLE_AUTHORISATION_STATUS,
  CONNECTION_ALREADY_EXIST,
  EXPECTED_FILE_NOT_FOUND,
  SDK_RESET_UNEXPECTEDLY,
  LOCAL_PUSH_NOTIFICATION_PERMISSION_NOT_GIVIN,
  BIOID_NOT_AVAILABLE,
  BIO_ID_SERVER_ERROR,
  BIO_ID_NOT_RECOGNIZED,
  BATTERY_LOW,
  FIRMWARE_DOWNLOAD_FAILED,
  FIRMWARE_UPDATE_CHECK_FAILED,
  DEVICE_ACTIVATION_FAILED,
  FIRMWARE_CRC_MISMATCH,
  FIRMWARE_VERIFICATION_FAILED,
}

ErrorType mapToErrorType(Object? event) {
  switch (event) {
    case 1000:
      return ErrorType.UNKNOWN_ERROR;
    case 1001:
      return ErrorType.INVALID_BLE_AUTHORISATION_STATUS;
    case 1003:
      return ErrorType.CONNECTION_ALREADY_EXIST;
    case 1004:
      return ErrorType.EXPECTED_FILE_NOT_FOUND;
    case 1005:
      return ErrorType.SDK_RESET_UNEXPECTEDLY;
    case 1006:
      return ErrorType.LOCAL_PUSH_NOTIFICATION_PERMISSION_NOT_GIVIN;
    case 1007:
      return ErrorType.BIOID_NOT_AVAILABLE;
    case 1008:
      return ErrorType.BIO_ID_SERVER_ERROR;
    case 1009:
      return ErrorType.BIO_ID_NOT_RECOGNIZED;
    case 1010:
      return ErrorType.BATTERY_LOW;
    case 1011:
      return ErrorType.FIRMWARE_DOWNLOAD_FAILED;
    case 1012:
      return ErrorType.FIRMWARE_UPDATE_CHECK_FAILED;
    case 1013:
      return ErrorType.DEVICE_ACTIVATION_FAILED;
    case 1014:
      return ErrorType.FIRMWARE_CRC_MISMATCH;
    case 1015:
      return ErrorType.FIRMWARE_VERIFICATION_FAILED;
    default:
      return ErrorType.UNKNOWN_ERROR;
  }
}

class BioError {
  BioError({
    required this.code,
    required this.message,
  });

  final int code;
  final String message;

  ErrorType get type => mapToErrorType(code);

  factory BioError.fromMap(Map<Object?, Object?> map) => BioError(
        code: map['code'] as int,
        message: map['message'] as String,
      );

  @override
  String toString() {
    return 'Error: $code message: $message';
  }
}
