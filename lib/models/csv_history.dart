class CSVHistory {
  CSVHistory({required this.bioId, this.path});

  final String bioId;
  final String? path;

  factory CSVHistory.fromMap(Map<Object?, Object?> map) => CSVHistory(
        bioId: map['bioId'] as String,
        path: map['path'] as String?,
      );

  @override
  String toString() {
    return 'CSV: bioId: $bioId path: $path';
  }
}
