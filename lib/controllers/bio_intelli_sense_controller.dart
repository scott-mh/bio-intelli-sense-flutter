import 'dart:async';

import 'package:biointellisense/models/activation_status.dart';
import 'package:biointellisense/models/bio_device.dart';
import 'package:biointellisense/models/bio_error.dart';
import 'package:biointellisense/models/csv_history.dart';
import 'package:biointellisense/models/device_history.dart';
import 'package:biointellisense/models/firmware.dart';
import 'package:biointellisense/models/scan_mode.dart';
import 'package:biointellisense/models/service_status.dart';
import 'package:biointellisense/models/connection_state.dart';
import 'package:flutter/foundation.dart';

import 'package:flutter/services.dart';

class Companion {
  Companion._();

  static const String serviceStatusChannel = 'BioIntelliSense/ServiceStatus';
  static const String connectionStateChannel = 'BioIntelliSense/ConnectionState';
  static const String deviceChannel = 'BioIntelliSense/device';
  static const String scanStateChannel = 'BioIntelliSense/ScanState';
  static const String errorChannel = 'BioIntelliSense/error';
  static const String verifyBioIdChannel = 'BioIntelliSense/verifyBioId';
  static const String activationChannelName = 'BioIntelliSense/activation';
  static const String firmwareChannelName = 'BioIntelliSense/firmware';
  static const String bioHubIdChannelName = 'BioIntelliSense/bioHubId';
  static const String lastSyncTimeChannelName = 'BioIntelliSense/lastSyncTime';
  static const String syncHistoryCSVChannelName = 'BioIntelliSense/syncHistoryCSV';
  static const String methodChannel = 'BioIntelliSense';
  static const String startServiceMethod = 'startService';
  static const String stopServiceMethod = 'stopService';
  static const String startScanMethod = 'startScanManual';
  static const String startScanForDeviceMethod = 'startScanForDevice';
  static const String stopScanMethod = 'stopScan';
  static const String isScanningMethod = 'isScanning';
  static const String retrieveQueuedDevicesMethod = 'retrieveQueuedDevices';
  static const String retrievePreviouslyConnectedDevicesMethod =
      'retrievePreviouslyConnectedDevices';
  static const String retrieveDeviceHistoryMethod = 'retrieveDeviceHistory';
  static const String retrieveConnectionStateMethod = 'retrieveConnectionState';
  static const String retrievePairedDeviceMethod = 'retrievePairedDevice';
  static const String setScanModeMethod = 'setScanMode';
  static const String verifyBioIdMethod = 'verifyBioId';
  static const String retrieveBioHubIdMethod = 'retrieveBioHubId';
  static const String retrieveLastSyncTime = 'retrieveLastSyncTime';
  static const String retrieveFirmwarePlatformVersionMethod =
      'retrieveFirmwarePlatformVersion';
  static const String retrieveTargetPlatformVersionMethod =
      'retrieveTargetPlatformVersion';
  static const String retrieveTargetSDKVersionMethod = 'retrieveTargetSDKVersion';
  static const String retrieveDeviceConfigurationNameMethod =
      'retrieveDeviceConfigurationName';
  static const String generateCSVHistoryMethod = 'generateCSVHistory';
  static const enableNotificationMethod = 'enableNotification';
  static const isNotificationChannelDisabledMethod = 'isNotificationChannelDisabled';
  static const resetMethod = 'reset';
  static const syncTimeWithServerMethod = 'syncTimeWithServer';
  static const deviceHistoryChannelName = 'BioIntelliSense/deviceHistory';
  static const firmwareAvailabilityChannelName = "BioIntelliSense/firmwareAvailability";
}

class BioIntelliSenseController {
  final _serviceStatusChannel = const EventChannel(Companion.serviceStatusChannel);
  final _connectionStateChannel = const EventChannel(Companion.connectionStateChannel);
  final _deviceChannel = const EventChannel(Companion.deviceChannel);
  final _scanStateChannel = const EventChannel(Companion.scanStateChannel);
  final _verifyBioIdChannel = const EventChannel(Companion.verifyBioIdChannel);
  final _activationChannel = const EventChannel(Companion.activationChannelName);
  final _firmwareChannel = const EventChannel(Companion.firmwareChannelName);
  final _bioHubIdChannel = const EventChannel(Companion.bioHubIdChannelName);
  final _lastSyncTimeChannel = const EventChannel(Companion.lastSyncTimeChannelName);
  final _csvHistoryChannel = const EventChannel(Companion.syncHistoryCSVChannelName);
  final _deviceHistoryChannel = const EventChannel(Companion.deviceHistoryChannelName);
  final _firmwareAvailabilityChannel =
      const EventChannel(Companion.firmwareAvailabilityChannelName);

  final _errorChannel = const EventChannel(Companion.errorChannel);

  static const MethodChannel _methodChannel =
      const MethodChannel(Companion.methodChannel);

  Stream<ServiceStatus> get serviceStatus => _serviceStatusChannel
      .receiveBroadcastStream()
      .distinct()
      .map((event) => mapToServiceStatus(event));

  Stream<ConnState> get connectionState => _connectionStateChannel
      .receiveBroadcastStream()
      .distinct()
      .map((event) => mapToConnectionState(event));

  Stream<DeviceEventData> get deviceEvents => _deviceChannel
      .receiveBroadcastStream()
      .distinct()
      .map((event) => DeviceEventData.fromMap(event));

  Stream<bool> get scanState =>
      _scanStateChannel.receiveBroadcastStream().distinct().map((event) => event == true);

  Stream<BioError> get errors => _errorChannel
      .receiveBroadcastStream()
      .distinct()
      .map((event) => BioError.fromMap(event));

  Stream<VerifyBioID> get verifyBioIdResult => _verifyBioIdChannel
      .receiveBroadcastStream()
      .distinct()
      .map((event) => VerifyBioID.fromMap(event));

  Stream<ActivationStatus> get activation => _activationChannel
      .receiveBroadcastStream()
      .distinct()
      .map((event) => ActivationStatus.fromMap(event));

  Stream<Firmware> get firmware => _firmwareChannel
      .receiveBroadcastStream()
      .distinct()
      .map((event) => Firmware.fromMap(event));

  Stream<CSVHistory> get csvHistory => _csvHistoryChannel
      .receiveBroadcastStream()
      .distinct()
      .map((event) => CSVHistory.fromMap(event));

  Stream<String?> get bioHubId =>
      _bioHubIdChannel.receiveBroadcastStream().distinct().map((event) => event);

  Stream<int?> get lastSyncTime =>
      _lastSyncTimeChannel.receiveBroadcastStream().distinct().map((event) => event);

  Stream<List<DeviceHistory>> get deviceHistory =>
      _deviceHistoryChannel.receiveBroadcastStream().distinct().map((event) {
        final list = <DeviceHistory>[];
        final data = event['events'];
        data.forEach((map) => list.add(DeviceHistory.fromMap(map)));
        return list;
      });

  Stream<FirmwareAvailability> get firmwareAvailability => _firmwareAvailabilityChannel
      .receiveBroadcastStream()
      .distinct()
      .map((event) => FirmwareAvailability.fromMap(event));

  static Future<void> startService() async {
    await _methodChannel.invokeMethod(Companion.startServiceMethod);
  }

  static Future<void> stopService() async {
    await _methodChannel.invokeMethod(Companion.stopServiceMethod);
  }

  static Future<void> startScanManual() async {
    await _methodChannel.invokeMethod(Companion.startScanMethod);
  }

  static Future<void> stopScan() async {
    await _methodChannel.invokeMethod(Companion.stopScanMethod);
  }

  static Future<bool> isScanning() async {
    return await _methodChannel.invokeMethod(Companion.isScanningMethod);
  }

  static Future<void> retrieveQueuedDevices() async {
    await _methodChannel.invokeMethod(Companion.retrieveQueuedDevicesMethod);
  }

  static Future<void> retrievePreviouslyConnectedDevices() async {
    await _methodChannel.invokeMethod(Companion.retrievePreviouslyConnectedDevicesMethod);
  }

  static Future<void> retrieveDeviceHistory(String bioId) async {
    await _methodChannel.invokeMethod(Companion.retrieveDeviceHistoryMethod, {
      'bioId': bioId,
    });
  }

  static Future<ConnState> retrieveConnectionState() async {
    return mapToConnectionState(
      await _methodChannel.invokeMethod<String>(Companion.retrieveConnectionStateMethod),
    );
  }

  static Future<BioDevice?> retrievePairedDevice() async {
    final data = await _methodChannel
        .invokeMethod<Map<Object?, Object?>>(Companion.retrievePairedDeviceMethod);
    if (data != null) {
      return BioDevice.fromMap(data);
    }
    return null;
  }

  static Future<void> setScanMode(ScanMode mode) async {
    await _methodChannel.invokeMethod<String>(Companion.setScanModeMethod, {
      'mode': describeEnum(mode),
    });
  }

  static Future<void> verifyBioId(String bioId) async {
    await _methodChannel.invokeMethod<String>(Companion.verifyBioIdMethod, {
      'bioId': bioId,
    });
  }

  static Future<void> retrieveDevicehistory(String bioId) async {
    await _methodChannel.invokeMethod<String>(Companion.retrieveDeviceHistoryMethod, {
      'bioId': bioId,
    });
  }

  static Future<void> startScanForDevice(String bioId, String deviceId,
      {bool isManual = false}) async {
    await _methodChannel.invokeMethod<String>(Companion.startScanForDeviceMethod, {
      'bioId': bioId,
      'deviceId': deviceId,
      'isManual': isManual,
    });
  }

  static Future<void> retrieveBioHubId() async {
    await _methodChannel.invokeMethod<String>(Companion.retrieveBioHubIdMethod);
  }

  static Future<void> retrieveLastSyncTime(String bioId) async {
    await _methodChannel.invokeMethod<String>(Companion.retrieveLastSyncTime, {
      'bioId': bioId,
    });
  }

  static Future<String?> retrieveFirmwarePlatformVersion() async {
    return await _methodChannel
        .invokeMethod<String?>(Companion.retrieveFirmwarePlatformVersionMethod);
  }

  static Future<String?> retrieveTargetPlatformVersion() async {
    return await _methodChannel
        .invokeMethod<String?>(Companion.retrieveTargetPlatformVersionMethod);
  }

  static Future<String?> retrieveTargetSDKVersion() async {
    return await _methodChannel
        .invokeMethod<String?>(Companion.retrieveTargetSDKVersionMethod);
  }

  static Future<String?> retrieveDeviceConfigurationName() async {
    return await _methodChannel
        .invokeMethod<String?>(Companion.retrieveDeviceConfigurationNameMethod);
  }

  static Future<void> generateCSVHistory(String bioId) async {
    return await _methodChannel.invokeMethod(Companion.generateCSVHistoryMethod, {
      'bioId': bioId,
    });
  }

  static Future<void> enableNotification({bool enable = false}) async {
    return await _methodChannel.invokeMethod(Companion.enableNotificationMethod, {
      'enable': enable,
    });
  }

  static Future<bool?> isNotificationChannelDisabled() async {
    return await _methodChannel
        .invokeMethod<bool>(Companion.isNotificationChannelDisabledMethod);
  }

  static Future<void> reset({bool clearSyncHistory = false}) async {
    return await _methodChannel.invokeMethod(Companion.resetMethod, {
      'clearSyncHistory': clearSyncHistory,
    });
  }

  static Future<void> syncTimeWithServer() async {
    return await _methodChannel.invokeMethod(Companion.syncTimeWithServerMethod);
  }
}
